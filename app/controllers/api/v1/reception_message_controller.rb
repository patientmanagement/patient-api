class Api::V1::ReceptionMessageController < ApplicationController
def create
      @reception_message=ReceptionMessage.new(reception_message)
      if @reception_message.save 
        render json: {reception_message:@reception_message,message:"new message has created  successfully"}, status: 200
      else
        render json: { errors: "server_error"}, status: 201
      end
  end
  def delete
    @reception_message = ReceptionMessage.find_by_id(params[:id])
    if @reception_message
        if @reception_message.destroy
          render json: {reception_message:@reception_message,message:"success deleting"}, status: 200
        else
          render json: { errors: "no found" }, status: 201
        end
    else
      render json: { errors: "no found" }, status: 201
    end
  end
  ## Get all reception messages
  def reception_messages
    @reception_messages = ReceptionMessage.where(:reception_id => params[:id])
    if @reception_messages != []
      @messages = Array.new
      @message = {}
      for message in @reception_messages
            message = message.attributes.merge(sender: User.find_by_id(message[:sender_id]))
            @messages << message
          end 
      render json: {reception_messages:@messages,message:'message found successfully'}, status: 200
      # render json: {reception_messages:@reception_messages,message:'message found success'}, status: 200
    else
      render json: { reception_messages: [] }, status: 201
    end
  end
  ## Get all receptions messages
  def receptions_messages
    @receptions_messages = ReceptionMessage.all
    if @receptions_messages != []
      render json: {reception_messages:@receptions_messages,message:'message found success]'}, status: 200
    else
      render json: { errors: "no_found_s" }, status: 201
    end
  end

  private

  def reception_message
   params.require(:message).permit(:staff_id, :user_id, :sender_id, :reception_id, :body)
  end


end
