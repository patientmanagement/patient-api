AWS.config(
  :access_key_id => 'AKIAJG3FAZ6DBPS37FNQ',
  :secret_access_key => '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')

class Date
  def self.parsable?(string)
    begin
      parse(string)
      true
    rescue ArgumentError
      false
    end
  end
end
def message 
     @data = Rails.configuration.configData.content
     return @data[:message][:users]
end
class Api::V1::UsersController < ApplicationController
  # before_filter :authenticate_request! , :except => [:create, :reset_pass_link, :reset_pass, :find_staffmembers]	
  # before_filter :authenticate_request!

  ## This project
  respond_to :json
  skip_before_filter  :verify_authenticity_token

  # Show user by id 
  def show
    @user = User.find_by_id(params[:id])
    if @user
      @fields = RegisterFieldValue.where(user_id: @user.id)
      @fields_info = []
      @fields.each do |field|
        @field_info = field.as_json
        @field_info[:register_field] = field.register_field_data
        @fields_info << @field_info
      end
      @user_info = @user.as_json
      @user_info[:fields] = @fields_info
      render json: @user_info
    else
      render json: {message: "User not found"}
    end
  end

  def get_payment_cred
    user = User.find_by(email: params[:email])
    if !user
      return render json: { errors: "User not found" }
    end
    if user.user_type != 'admin'
      return render json: { errors: "User is not admin" }
    end
    if !user.company 
      return render json: { errors: "Company not found" }
    end
    if user.company.subdomain != params[:subdomain]
      return render json: { errors: "User is not from current company" }
    end
    payment = Payment.where(user_id: user.id).order(:created_at).last
    if !payment
      return render json: { errors: "User has no payments" }
    end
    return render json: { user_id: user.id, plan_id: payment.plan_id }
  end

  # Show all users 
  def show_all
    if params[:company_id]
      # company_users = CompanyUser.where(company_id: params[:company_id])
      company = Company.find_by_id(params[:company_id])
      if company

        if params[:is_active] == 'not_active'
          return render json: company.user.where(is_active: '0', user_type: 'user').count
        end

        if params[:user_type]
          return render json: company.user.where(user_type: params[:user_type])
        end

        return render json: company.user
      else
        return render json: { errors: "Company not found" }
      end
    else
      # if params[:users_for_superadmin]
      #   response = User.where(user_type: 'admin')
      # end
      return render json: User.all
    end
    # # render json: company_user.user
    

    
    # if company_users && params[:is_active] == 'not_active'
    #   @users = Array.new
    #   for company_user in company_users
    #     @users << User.find(company_user.user_id)
    #   end
    #   for user in @users
    #     # puts user.is_active;
    #     if (user.user_type == 'user' && user.is_active == '0')
    #       response << user
    #     end
    #   end
    #   response = response.length
    # end
    

    # if company_users && params[:user_type]
    #   @users = Array.new
    #   for company_user in company_users
    #     @users << User.find(company_user.user_id)
    #   end
    #   for user in @users
    #     if user.user_type == params[:type]
    #       response << user
    #     end
    #   end
    # end

    # if response
    #   render json: response
    # else
    #   render json: {message: "Users not found"}
    # end

    # @users = User.all
    # @users = @users.where(user_type: 'user') #TODO: REMOVE IT!!!!

    # if params[:is_active] == 'not_active'
    #   @users = @users.where(user_type: 'user', is_active: '0').count
    # end

    # if params[:user_type]
    #   @users = @users.where(user_type: params[:user_type])
    # end
    # # @users = User.where(user_type: 'user', is_active: '0')
    # # @users = User.all
    # if @users
    #   render json: @users
    # else
    #   render json: {message: "Users not found"}
    # end
    
  end

  def show_all_staffmember
    # company_users = CompanyUser.where(company_id: params[:company_id])
    # @users = Array.new
    # for company_user in company_users
    #   user = User.find(company_user.user_id)
    #   if (user.user_type == 'staffmember')
    #     @users << user
    #   end
      
    # end
    # # @users = User.where(user_type: 'staffmember')
    # if @users
    #   render json: @users
    # else
    #   render json: {message: "Users not found"}
    # end
    if params[:company_id]

      company = Company.find_by_id(params[:company_id])

      if company
        return render json: company.user.where(user_type: 'staffmember')
      else
        return render json: { errors: "Company not found" }
      end

    else
      return render json: User.where(user_type: 'staffmember')
    end

  end

  def show_all_users
    # company_users = CompanyUser.where(company_id: params[:company_id])
    # @users = Array.new
    # for company_user in company_users
    #   @users << User.find(company_user.user_id)
    # end
    # # @users = User.where(user_type: params[:type])
    # if @users
    #   render json: @users
    # else
    #   render json: {message: "Users not found"}
    # end

    if params[:company_id]

      company = Company.find_by_id(params[:company_id])
      
      if company
        return render json: company.user.where(user_type: params[:type])
      else
        return render json: { errors: "Company not found" }
      end

    else
      return render json: User.where(user_type: 'staffmember')
    end
    
  end
  def user_friends  
    @users = Array.new
    user_type = User.find_by_id(params[:id])
    case user_type[:user_type]
        when 'user'
          @reception_messages = ReceptionMessage.where(:user_id => params[:id]) 
          for message in @reception_messages
              @user_id = User.find_by_id(message.staff_id)
              unless @users.include? @user_id
                @users << @user_id
              end
          end 
      when 'staffmember'
          @reception_messages = ReceptionMessage.where(:staff_id => params[:id])
          for message in @reception_messages
              @user_id = User.find_by_id(message.user_id)
              unless @users.include? @user_id
                @users << @user_id
              end
          end  
    end
          @friends = Array.new
          for id in @users
              @user = User.find_by_id(id)
              @friends << @user
          end 
    render json: { friends: @friends}, status: 200
  end
  def user_chat  
    # @receptions = Array.new
    @user_chat = Array.new
    @chat = {}

    user = User.find_by_id(params[:id])
    case user[:user_type]
        when 'user'
          @receptions = Reception.where(:user_id => params[:id]) 
          for reception in @receptions
              @reception_messages = ReceptionMessage.where(:reception_id => reception[:id]).last
              if @reception_messages
                @chat.store("user", User.find_by_id(reception[:staff_id]))
                @chat.store("reception", reception)
                @chat.store("message", @reception_messages)
                @user_chat << @chat
                @chat = {}
              end
          end 
      when 'staffmember'
          @receptions = Reception.where(:staff_id => params[:id]) 
          for reception in @receptions
              @reception_messages = ReceptionMessage.where(:reception_id => reception[:id]).last
              if @reception_messages
                @chat.store("user", User.find_by_id(reception[:user_id]))
                @chat.store("reception", reception)
                @chat.store("message", @reception_messages)
                @user_chat << @chat
                @chat = {}
              end
          end  
    end
          
    render json: { chat: @user_chat}, status: 200
  end
  def create
    @user=User.new(user_params)
    @company = Company.where(subdomain: params[:subdomain]).take
    if @company || user_params[:user_type] == "superadmin" || user_params[:user_type] == "admin"
      if @user.save
        if user_params[:user_type] != "superadmin" && user_params[:user_type] != "admin"
          company_user = CompanyUser.new(company_id: @company.id, user_id: @user.id)
          company_user.save
          UserMailer.registration_confirmation(@user, "").deliver_now 
        end
        if params[:fields]
          params[:fields].each do |field|
            @registered_field = RegisterFieldValue.new({register_field_id: field[:id], user_id: @user.id, value: field[:value]})
            @registered_field.save
          end
        end

        
        render json: @user, status: 200
      else
        render json: { errors: @user.errors}, status: 200
      end
    else
      render json: { errors: {company: "Please select company"} }
    end
  end

  def change_password
      @user = User.find_by_id(params[:id])
      if @user
        if @user.update(change_pass_params)
             render json: { success: "Password was successfully changed!"}, status: 200
        else
          render json: { error: @user.errors }
        end
      else
        render json: { error: "User not found." }
      end
  end

  # Updating users
  def update
      @user = User.find_by_id(params[:id])
      if @user
        if @user.update(user_update_params)
          if (@user.user_type == 'staffmember' && params[:speciality]) 
            @user_speciality = @user.speciality
            if (@user_speciality)
              @speciality = Speciality.find(@user_speciality[:id])
              if @speciality
                @speciality.update(:speciality => params[:speciality])
              end
            else
              @speciality = Speciality.new(:user => @user, :speciality => params[:speciality])
              @speciality.save
            end
          end
          if params[:user][:fields]
            params[:user][:fields].each do |field|
              @field = RegisterFieldValue.find(field[:id])
              @field.update({value: field[:value]})
            end
          end
          @fields = RegisterFieldValue.where(user_id: @user.id)
          @fields_info = []
          @fields.each do |field|
            @field_info = field.as_json
            @field_info[:register_field] = field.register_field_data
            @fields_info << @field_info
          end
          @user = User.find_by_id(params[:id])
          @user_info = @user.as_json
          @user_info[:fields] = @fields_info
          render json: @user_info, status: 200
        else
          render json: { errors: @user.errors }, status: 200
        end
      else
        render json: { errors: "User not found" }, status: 200
      end
  end
    # verified_user 
  def verified_user
      user = User.find_by_id(params[:id])
      if user
        if user.update({is_verified: '1'})
          render json: {email: user.email}, status: 200
        else
          render json: { errors: user.errors }, status: 200
        end
      else
        render json: { errors: "User not found" }, status: 200
      end
  end

  # activate_user 
  def activate_user
      user = User.find_by_id(params[:id])
      if user
        if user.update({is_active: '1'})
          if user.is_active == '1'
            UserActivateMessage.user_activate_message(user).deliver_now  
          end
          render json: user, status: 200
        else
          render json: { errors: user.errors }, status: 200
        end
      else
        render json: { errors: "User not found" }, status: 200
      end
  end

  # Deleting users
  def destroy
    user = User.find_by_id(params[:id])
    if user
      if user.destroy
        render json: { success: "User successfully deleted" }
      else
        render json: { success: "User not successfully deleted" }
      end
    else
      render json: { success: "User not found" }
    end
  end

  def create_staff

    @user = User.where(email: user_params[:email]).take
    if !@user
        @company = Company.find(params[:company_id])
        unless @company.nil?
          @user_id = @company.user_id
        else
          return render json: { errors: "No such company" }
        end
        @last_payment = Payment.where(user_id: @user_id).last
        if @last_payment.nil?
          return render json: { errors: "Company didn't choose plan yet" }
        end
        @plan = Plan.find(@last_payment.plan_id)
        @user_staff_relations = UsersStaffRelation.where(user_id: @user_id)
        if @user_staff_relations.length < @plan.staff_members
          # @generated_password = Devise.friendly_token.first(20)
          @generated_password = SecureRandom.urlsafe_base64(20)

          @staff_params ={
                first_name: user_params[:first_name],
                last_name: user_params[:last_name],
                email: user_params[:email],
                is_active: '1',
                user_type: user_params[:user_type],
                password: @generated_password
          }

          @user=User.new(@staff_params)

          if @user.save
            if params[:company_id]
              company_user = CompanyUser.new(company_id: params[:company_id], user_id: @user.id)
              company_user.save
            end
            @user_staff_relation=UsersStaffRelation.new({user_id: params[:id] , staff_id: @user.id})
            @user_staff_relation.save
            UserMailer.registration_confirmation(@user, @generated_password).deliver_now 
            render json: @user, status: 200
          else
            render json: { errors: @user.errors}, status: 200
          end
        else
          render json: { errors: "You have already registered maximum number of staff members" }
        end
    else
      render json: { errors: "This email is registered." }
    end      
  end

  # Show all user staff relation 
  def show_all_staff
    @user_staff_relations = UsersStaffRelation.where(user_id: params[:id])
    @users = Array.new
    for relation in @user_staff_relations
      @user = User.find(relation.staff_id)
      @users << @user
    end  
    render json: @users
  end

  def show_staff
    user_staff_rel = UsersStaffRelation.where(user_id: params[:id], staff_id: params[:staff_id]).take
    user = User.find_by_id(params[:staff_id])
    if user_staff_rel && user
    render json: user
    else
    render json: {mesage: 'Staff user not found.'}
    end
  end

  def find_staffmember
    
    # @working_hours = WorkingHour.where(day: params[:day])

    @user_working_hours = UserWorkingHour.where( user_id: params[:id])
    @user = User.find_by_id(params[:id])

    @users = Array.new
    @get_staff= {}
    for w_hour in @user_working_hours
      @working_hour = WorkingHour.where(id: w_hour.working_hours_id).take
         if @working_hour.day == params[:day]
            @get_staff.store("break_time", @working_hour)
            
            @staff_work_time = {start_time: @working_hour.start_time, end_time: @working_hour.end_time}
            @get_staff.store("profile", @user)
            @get_staff.store("work_time", @staff_work_time)
            date = Date.parse(params[:time])
            #break
            @user_break = Break.where(user_id: params[:id]).where("end_repeat >= ?", params[:time])
            @get_staff.store("break_time", @user_break)

            ##Reception
            @reception=Reception.where(staff_id: params[:id]).where(end_date: date.midnight..date.end_of_day).where(status: 2)
            @get_staff.store("receptions", @reception)

            @users << @get_staff
            @get_staff= {}
         end 
         
      

    end  
     render json: @users
     # render json: Time.zone.now.to_date

    
  end
  def find_staffmembers
    
     # @working_hours = WorkingHour.where(day: params[:day])

    @working_hours = Array.new
    current_user.company.user.where(user_type: 'staffmember').find_each do |user|
     @user_working_hour = user.working_hour.find_by(day: params[:day])
     @working_hours << @user_working_hour if @user_working_hour
    end

    @users = Array.new
    @get_staff= {}
    for working_hour in @working_hours
      @user_working_hour = UserWorkingHour.where( working_hours_id: working_hour.id).take
      #@user_working_hour = UserWorkingHour.find(working_hour.id)
      @user = User.find_by_id(@user_working_hour.user_id)
      @staff_work_time = {start_time: working_hour.start_time, end_time: working_hour.end_time}
      @get_staff.store("profile", @user)
      @get_staff.store("work_time", @staff_work_time)
      date = Date.parse(params[:time])
      #break
      @user_break = Break.where(user_id: @user_working_hour.user_id).where("end_repeat >= ?", params[:time])#.where(start_day: date.midnight..date.end_of_day)
      @get_staff.store("break_time", @user_break)

      ##Reception
      @reception=Reception.where(staff_id: @user_working_hour.user_id).where(end_date: date.midnight..date.end_of_day).where(status: 2)
      @get_staff.store("receptions", @reception)
      @users << @get_staff
      @get_staff= {}
    end  
     render json: @users
     # render json: Time.zone.now.to_date

    
  end

  def update_staff
    user = User.find_by_id(params[:staff_id])
    user_staff_rel = UsersStaffRelation.where(user_id: params[:id], staff_id: params[:staff_id]).take
    @staff_params ={
            first_name: user_params[:first_name],
            last_name: user_params[:last_name],
            # email: user_params[:email],
            is_active: user_params[:is_active],
            user_type: user_params[:user_type],
            password: @generated_password
    }

    if user.update(@staff_params)
      render json: user, status: 200
    else
      render json: { errors: user.errors }, status: 200
    end
  end

  # Deleting users
  def destroy_staff
    user_staff_rel = UsersStaffRelation.where(user_id: params[:id] , staff_id: params[:staff_id]).take
    user = User.find_by_id(params[:staff_id])
    if user && user_staff_rel
      company_user = CompanyUser.where(user_id: user.id).take
      if company_user
        company_user.destroy
      end

      user_staff_rel.destroy
      user.destroy
      render json: { message: "Staff was successfully deleted." }, status: 200
    else
      render json: { message: "Staff was not successfully deleted, please correct user_id or staff_id" }, status: 500
    end
  end

  def reset_pass_link
    @user = User.where(email: user_params[:email]).take  
    if @user 
      ResetPassMailer.reset_pass_confirmation(@user).deliver_now 
      render json: { message: 'Email have sent successfully, please go to your email',  email: @user.email}, status: 200
    else
      render json: { errors: 'User not found'}, status: 200
    end
  end

  def reset_pass
    @user = User.where(email: user_params[:email]).take
    if @user
      if @user.update(change_pass_params)
           render json: { success: "Password was successfully changed!"}, status: 200
      else
        render json: { errors: @user.errors }, status: 200
      end
    else
      render json: { errors: "User not found." }
    end
  end


  def create_working_hours
      @user_working_hours_rel = UserWorkingHour.where(user_id: params[:user_id])
      if @user_working_hours_rel[0]
        for relation in @user_working_hours_rel
          @user_working_hours = WorkingHour.where(id: relation.working_hours_id).take
          if @user_working_hours.day == working_hours_params[:day]
            @work_day = @user_working_hours
          else
            @work_day = {}
          end
        end
      else
         @work_day = {}
      end  

      if @work_day == {}
         @working_hours=WorkingHour.new(working_hours_params)
            if @working_hours.save
              @user_working_hours_rel=UserWorkingHour.new({user_id: params[:user_id] , working_hours_id: @working_hours.id})
              @user_working_hours_rel.save
              render json: @working_hours , status: 200
            else
              render json: { errors: @working_hours.errors}, status: 500
            end
      else
              render json: { errors: {day: "working hours for " + working_hours_params[:day] + " are registered"}}, status: 422 
      end    
  end

  def create_breaks
    user = User.find_by_id(params[:user_id])
    if user
      if break_time_params[:repeat_type] == 'none'
        @end_repeat = break_time_params[:end_day]
      else
        @end_repeat = break_time_params[:end_repeat] 
      end

      @break_param = {
        user_id: params[:user_id],
        start_day: break_time_params[:start_day],
        end_day: break_time_params[:end_day],
        end_repeat: @end_repeat,
        repeat_type: break_time_params[:repeat_type]
      }

      @break=Break.new(@break_param)
      if @break.save
         @user_break_rel=UserBreak.new({user_id: params[:user_id] , break_id: @break.id})
         @user_break_rel.save
         render json: @break , status: 200
      else
        render json: { errors: @break.errors}, status: 200
      end
    else
      render json: { errors: "user not found."}, status: 200
    end
  end

  def get_woring_hours
      @user_woring_hours_rel = UserWorkingHour.where(user_id: params[:user_id])
      @woring_hours = Array.new
      for relation in @user_woring_hours_rel
        @woring_hour=WorkingHour.find_by_id(relation.working_hours_id)
        @woring_hours << @woring_hour
      #   case @working_hour.day
      #   when 'sunday'
      #     @working_hours[0] = @working_hour
      #   when 'monday'
      #     @working_hours[1] = @working_hour
      #   when 'tuesday'
      #     @working_hours[2] = @working_hour
      #   when 'wednesday'
      #     @working_hours[3] = @working_hour
      #   when 'thursday'
      #     @working_hours[4] = @working_hour
      #   when 'friday'
      #     @working_hours[5] = @working_hour
      #   when 'saturday'
      #     @working_hours[6] = @working_hour
      #   end    
      end
      render json: @woring_hours , status: 200
  end


  def get_breaks
      @user_break_rel = UserBreak.where(user_id: params[:user_id])
      @breaks = Array.new
      for relation in @user_break_rel
        @break=Break.find_by_id(relation.break_id)
        @breaks << @break   
      end
      render json: @breaks
  end

  def destroy_working_hours
    working_hours_rel = UserWorkingHour.where(user_id: params[:user_id] , working_hours_id: params[:working_hours_id]).take
    working_hours = WorkingHour.find_by_id(params[:working_hours_id])
    if working_hours && working_hours_rel
      working_hours_rel.destroy
      working_hours.destroy
      render json: { message: "Working Hours was successfully deleted." }, status: 200
    else
      render json: { message: "Working Hours was not successfully deleted, please correct user_id or work_time_id" }, status: 500
    end  
  end

  
  def delete_working_hours
    working_hours = WorkingHour.find_by_id(params[:id])
    if working_hours
      working_hours.destroy
      render json: { message: "Working Hours was successfully deleted." }, status: 200
    else
      render json: { message: "Working Hours was not successfully deleted, please correct user_id or work_time_id" }, status: 500
    end  
  end

  def destroy_break
    @user_break_rel = UserBreak.where(user_id: params[:user_id] , break_id: params[:break_id]).take
    @break = Break.find_by_id(params[:break_id])
    if @break && @user_break_rel
      @user_break_rel.destroy
      @break.destroy
      render json: { message: "Break was successfully deleted." }, status: 200
    else
      render json: { message: "Break was not successfully deleted, please correct user_id or brake_id" }, status: 500
    end  
  end

  def update_working_hours
    working_hours_rel = UserWorkingHour.where(user_id: params[:user_id] , working_hours_id: params[:working_hours_id]).take
    working_hours = WorkingHour.find_by_id(params[:working_hours_id])
    if working_hours && working_hours_rel      
       working_hours.update({start_time: working_hours_params[:start_time], end_time: working_hours_params[:end_time]})
       render json: working_hours, status: 200
    else
      render json: { message: "Required fields: day, start_time , end_time" }, status: 500
    end  
  end

  def update_break
    @user_break_rel = UserBreak.where(user_id: params[:user_id]).take
    @break = Break.find_by_id(params[:break_id])
    if @break && @user_break_rel 

      if break_time_params[:repeat_type] == 'none'
        @end_repeat = break_time_params[:end_day]
      else
        @end_repeat = break_time_params[:end_repeat] 
      end

      @break_param = {
        user_id: params[:user_id],
        start_day: break_time_params[:start_day],
        end_day: break_time_params[:end_day],
        end_repeat: @end_repeat,
        repeat_type: break_time_params[:repeat_type]
      }


       if  @break.update(@break_param)  
        render json: @break, status: 200
      else
        render json: { errors: @break.errors}, status: 200
      end
    else
      render json: { message: "Required fields: start_day, end_day, end_repeat, repeat_type" }, status: 200
    end  
  end

  def upload_logo_to_s3
     key = params['key']
     dell_key = params['dell_key']
     folder = params['folder']
     if key != '' && folder !=''
      s3 = AWS::S3.new
        bucket = 'patientusers'
        key = folder+'/'+key

        if dell_key != ''
          s3 = AWS::S3.new
          bucket = 'patientusers'
                b = s3.buckets[bucket]
                #delete S3 object 
                obj = b.objects[dell_key]
                obj.delete
        end

              b = s3.buckets[bucket]
              file = params['file']
              @company_user_relation = CompanyUser.where(user_id: folder).take
              @is_free_space = true;
              if @company_user_relation
                @is_free_space = check_taken_space(@company_user_relation.company_id, file.size)
              end
              if @is_free_space
                if file.path
                    path_to_file = file.path
                    file_size = file.size
                    file_exp = file.eof?
                    #create S3 object and save to current bucket
                            obj = b.objects[key].write(:file => path_to_file)
                            obj = obj.url_for(:read)
                    # add read permission to created object(file)
                    if obj.path
                          permission = b.objects[key]
                          permission.acl = :public_read
                    end

                    user = User.find_by_id(folder)
                    user.update({logo: key})

                    render json: {
                      bucket:bucket,
                      path:obj.path,
                      obj:obj,key:key,
                      exp:file_exp,
                      user: user
                      }, status: 201
                else 
                          render json: {params:params,path:'',obj:'',key:key,message:'empty file content'}, status: 500
                end
              else
                render json: { errors: "There are no enough space"}, status: 500
              end
    else
        render json: {params:params,path:'',obj:'',key:key,message:'empty file name'}, status: 500
    end    
  end

  def upload_file_to_s3
     key = params['key']
     r_id = params['reception_id']
     folder = params['folder']
     if key != '' && folder !=''
      s3 = AWS::S3.new
        bucket = 'patientusers'
        key = folder+'/'+key

              b = s3.buckets[bucket]
              file = params['file']
              @company_user_relation = CompanyUser.where(user_id: folder).take
              if check_taken_space(@company_user_relation.company_id, file.size)
                if file.path
                    path_to_file = file.path
                    file_size = file.size
                    file_exp = file.eof?
                    #create S3 object and save to current bucket
                            obj = b.objects[key].write(:file => path_to_file)
                            obj = obj.url_for(:read)
                    # add read permission to created object(file)
                    if obj.path
                          permission = b.objects[key]
                          permission.acl = :public_read
                    end
                 
                    file_params = {
                      user_id: folder,
                      reception_id: r_id,
                      url: key
                    }
                    @new_file = UserFile.new(file_params)
                    if @new_file.save
                      render json: {
                      bucket:bucket,
                      path:obj.path,
                      obj:obj,key:key,
                      exp:file_exp
                      }, status: 201
                    else
                      render json: { errors: @new_file.errors}, status: 410
                    end
                else 
                          render json: {params:params,path:'',obj:'',key:key,message:'empty file content'}, status: 500
                end
              else
                render json: { errors: "There are no enough space"}, status: 500
              end
    else
        render json: {params:params,path:'',obj:'',key:key,message:'empty file name'}, status: 500
    end    
  end

  def remove_file_from_s3
      key = params['key'] 
       if key != ''
        s3 = AWS::S3.new
          bucket = 'patientusers'
          b = s3.buckets[bucket]
          #delete S3 object 
          obj = b.objects[key]
          obj.delete

          render json: {params:params}, status: 201
      else   
         render json: {params:params,message:'empty file name'}, status: 201
      end
  end

  def get_receptions
    if params[:user_type] 
      case params[:user_type]
        when 'user'
          if params[:filter] == 'archive'
            @user_receptions = Reception.where("user_id = :id AND end_date <= :end_date", { id: params[:id], end_date: DateTime.now})
          elsif params[:filter] == 'upcoming'
            @user_receptions = Reception.where("user_id = :id AND start_date >= :start_date", { id: params[:id], start_date: DateTime.now})
          else
            @user_receptions = Reception.where(:user_id => params[:id])
          end

          @receptions = Array.new
          @get_receptions= {}

          for reception in @user_receptions

            @get_reception= {}
            @payment_status = PaymentHistory.where(reception_id:reception.id).take
            @reception_message = ReceptionMessage.where(:reception_id => reception.id).where(:staff_id => reception.staff_id).take
            @user = User.find_by_id(reception.staff_id)
            @staff_price = UserPrice.where(user_id:@user[:id]).take
            if @staff_price
               @price = @staff_price[:amount]
            else
               @price = 0
            end
            @get_reception.store("staff", @user)
            @get_reception.store("id", reception.id)
            @get_reception.store("staff_id", reception.staff_id)
            @get_reception.store("price", @price)
            @get_reception.store("message", @reception_message)
            @get_reception.store("user_id", reception.user_id)
            @get_reception.store("start_date", reception.start_date)
            @get_reception.store("end_date", reception.end_date)
            @get_reception.store("status", reception.status)
            @get_reception.store("payment_status", @payment_status)
            @receptions << @get_reception
        end

        when 'staffmember'   
          if params[:filter] == 'archive'
            @staff_receptions = Reception.where("staff_id = :id AND end_date <= :end_date", { id: params[:id], end_date: DateTime.now})
          elsif params[:filter] == 'upcoming'
            @staff_receptions = Reception.where("staff_id = :id AND start_date >= :start_date", { id: params[:id], start_date: DateTime.now})
          else
            @staff_receptions = Reception.where(:staff_id => params[:id])
          end
          
          @receptions = Array.new
          @get_receptions= {}

          for reception in @staff_receptions

            @get_reception= {}
            @payment_status = PaymentHistory.where(reception_id:reception.id).take
            # if params[:filter] == 'archive'
            #   if (reception.status != 2 || (reception.status == 2 && !@payment_status)) && (reception.status != 5)
            #     reception.update(:status => 5)
            #   end
            # end
            @reception_message = ReceptionMessage.where(:reception_id => reception.id).where(:user_id => reception.user_id).take
            @user = User.find_by_id(reception.staff_id)
            @user = User.find_by_id(reception.user_id)
            @get_reception.store("user", @user)
            @get_reception.store("id", reception.id)
            @get_reception.store("message", @reception_message)
            @get_reception.store("staff_id", reception.staff_id)
            @get_reception.store("user_id", reception.user_id)
            @get_reception.store("start_date", reception.start_date)
            @get_reception.store("end_date", reception.end_date)
            @get_reception.store("status", reception.status)
            @get_reception.store("payment_status", @payment_status)
            @receptions << @get_reception
        end
        else
          render json: { error:message[:incorrect_type_field]}, status: 200
          return;
        end
      
      
      if @receptions != []

        # render json: @receptions1
        render json: {receptions: @receptions, message: message[:user_receptions]} , status: 200
      else
        render json: { receptions: @receptions, message: message[:error_user_receptions]}, status: 200
      end
    else
      render json: { error:message[:empty_type_field]}, status: 200
    end
  end
  
  def add_user_price
    @user_price ={
      user_id:params[:user_id],
      amount:price_params[:amount]
    }
    @price=UserPrice.new(@user_price)
    if @price.save
      render json: {price:@price}, status: 200
    else
      render json: { errors: @price.errors}, status: 200
    end
  end
  def update_user_price
    @price=UserPrice.find_by_id(params[:id])
    if @price.update(price_params)
      render json: {price:@price}, status: 200
    else
      render json: { errors: @price.errors}, status: 200
    end
  end
  def delete_user_price
    @price=UserPrice.find_by_id(params[:id])
    if @price
      if @price.destroy
        render json: { success: "Price successfully deleted" }
      else
        render json: { success: "Price not successfully deleted" }
      end
    else
      render json: { success: "Price not found" }
    end
  end
  def get_user_price
    @price = UserPrice.where(user_id: params[:user_id]).take
    if @price
      render json: {price:@price}, status: 200
    else
      render json: {price: []}, status: 200
    end
    
  end

  # Admin Stripe credentials
  def add_admin_stripe_key
    Stripe.api_key =params[:key]
    stripeToken = Stripe::Token.create()
    rescue Stripe::InvalidRequestError, 
     Stripe::AuthenticationError,
     Stripe::APIConnectionError,
     Stripe::StripeError => e
      if(e.message.include?('Invalid API Key provided'))
        render json: {errors:{stripe:e.message}}, status: 200
        return
      end


    @admin_stripe_key ={
      user_id:params[:id],
      key:params[:key],
      is_active: true
    }
    @key=SripeCredential.new(@admin_stripe_key)
    @twocheckout_credentials = TwocheckoutCredential.where(user_id: params[:id]).take
    if @twocheckout_credentials
      @twocheckout_credentials.is_active = false;
      @twocheckout_credentials.save
    end
    if @key.save
      render json: {key:@key}, status: 200
    else
      render json: { errors: @key.errors}, status: 200
    end
  end
  def update_admin_stripe_key
      Stripe.api_key =params[:key]
      stripeToken = Stripe::Token.create()
      rescue Stripe::InvalidRequestError, 
       Stripe::AuthenticationError,
       Stripe::APIConnectionError,
       Stripe::StripeError => e
        if(e.message.include?('Invalid API Key provided'))
          render json: {errors:{stripe:e.message}}, status: 200
          return
        end
    
    @admin_stripe_key ={
      key:params[:key]
    } 
    @key=SripeCredential.find_by_id(params[:id])
    if @key.update(@admin_stripe_key)
      render json: {key:@key}, status: 200
    else
      render json: { errors: @key.errors}, status: 200
    end
  end
  def delete_admin_stripe_key
    @key=SripeCredential.find_by_id(params[:id])
    @twocheckout_credentials = TwocheckoutCredential.where(user_id: params[:id]).take
    if @twocheckout_credentials
      @twocheckout_credentials.is_active = true;
      @twocheckout_credentials.save
    end
    if @key
      if @key.destroy
        render json: { success: "Your stripe key successfully deleted" }
      else
        render json: { success: "Your stripe key have not deleted" }
      end
    else
      render json: { success: "Your stripe key not found" }
    end
  end
  def get_admin_stripe_key
    @key = SripeCredential.where(user_id: params[:user_id]).take
    if @key
      render json: {key:@key}, status: 200
    else
      render json: {key: []}, status: 200
    end
    
  end
  def get_all_stripe_key
    @key = SripeCredential.all
    if @key
      render json: {key:@key}, status: 200
    else
      render json: {key: []}, status: 200
    end
    
  end

  # Admin Paypal credentials
  def add_admin_paypal_credentials
    PayPal::SDK::REST.set_config(
      :mode => "sandbox", # "sandbox" or "live"
      :client_id => params[:client_id],
      :client_secret => params[:client_secret])

    @admin_paypal_credentials = {
      user_id: params[:user_id],
      client_id: params[:client_id],
      client_secret: params[:client_secret]
    }
    @credentials = PaypalCredential.new(@admin_paypal_credentials)
    if @credentials.save
      render json: {key:@credentials}, status: 200
    else
      render json: { errors: @credentials.errors}, status: 200
    end
  end

  def get_admin_paypal_credentials
    if params[:user_id] == "superadmin"
      @superadmin = User.where(user_type: "superadmin").take
      params[:user_id] = @superadmin.id
    end
    @credentials = PaypalCredential.where(user_id: params[:user_id]).take
    if @credentials
      render json: {credentials:@credentials}, status: 200
    else
      render json: {credentials: []}, status: 200
    end
  end

  def update_admin_paypal_credentials
    @admin_paypal_credentials = {
      client_id: params[:client_id],
      client_secret: params[:client_secret]
    } 
    @credentials = PaypalCredential.find_by_id(params[:id])
    if @credentials.update(@admin_paypal_credentials)
      render json: {credentials:@credentials}, status: 200
    else
      render json: { errors: @credentials.errors}, status: 200
    end
  end

  def delete_admin_paypal_credentials
    @credentials = PaypalCredential.find_by_id(params[:id])
    return render json: { success: @credentials }
    if @credentials
      if @credentials.destroy
        render json: { success: "Your paypal credentials were successfully deleted" }
      else
        render json: { success: "Your paypal credentials were not deleted" }
      end
    else
      render json: { success: "Your paypal credentials not found" }
    end
  end

  # Admin 2checkout credentials
  def add_admin_twocheckout_credentials
    Twocheckout::API.credentials = {
      :seller_id => params[:seller_id],
      :private_key => params[:private_key]
    }

    @admin_twocheckout_credentials = {
      user_id: params[:user_id],
      seller_id: params[:seller_id],
      private_key: params[:private_key],
      publishable_key: params[:publishable_key],
      is_active: true
    }
    @credentials = TwocheckoutCredential.new(@admin_twocheckout_credentials)
    @stripe_credentials = SripeCredential.where(user_id: params[:user_id]).take
    if @stripe_credentials
      @stripe_credentials.is_active = false;
      @stripe_credentials.save
    end
    if @credentials.save
      render json: { credentials: @credentials}, status: 200
    else
      render json: { errors: @credentials.errors}, status: 200
    end
  end

  def update_admin_twocheckout_credentials
    @admin_twocheckout_credentials = {
      user_id: params[:user_id],
      seller_id: params[:seller_id],
      private_key: params[:private_key],
      publishable_key: params[:publishable_key]
    }
    @credentials = TwocheckoutCredential.find_by_id(params[:id])
    if @credentials.update(@admin_twocheckout_credentials)
      render json: { credentials: @credentials}, status: 200
    else
      render json: { errors: @credentials.errors}, status: 200
    end
  end

  def delete_admin_twocheckout_credentials
    @credentials = TwocheckoutCredential.find_by_id(params[:id])
    @stripe_credentials = SripeCredential.where(user_id: params[:user_id]).take
    if @stripe_credentials
      @stripe_credentials.is_active = true;
      @stripe_credentials.save
    end
    if @credentials
      if @credentials.destroy
        render json: { success: "Your stripe key successfully deleted" }
      else
        render json: { success: "Your stripe key have not deleted" }
      end
    else
      render json: { success: "Your stripe key not found" }
    end
  end

  def get_admin_twocheckout_credentials
    if params[:user_id] == "superadmin"
      @superadmin = User.where(user_type: "superadmin").take
      params[:user_id] = @superadmin.id
    end
    if params[:subdomain]
      @company = Company.where(subdomain: params[:subdomain]).take
      params[:user_id] = @company.user_id
    end
    @credentials = TwocheckoutCredential.where(user_id: params[:user_id]).take
    if @credentials
      render json: { credentials: @credentials }, status: 200
    else
      render json: { credentials: [] }, status: 200
    end
  end

  def get_statistics
    @user = User.find_by_id(params[:id])
    if !@user
      return render json: { errors: "User not found" }
    end

    case @user[:user_type]
    when 'admin'
      render json: admin_statistic(@user)
    when 'staffmember'
      render json: staff_statistic(@user)
    when 'superadmin'
      render json: superadmin_statistic(@user)
    end
  end

  def test
    # return render json: { user: current_user, receptions: current_user.reception }

    # reception = Reception.find(15)
    # UserMailer.end_of_membership(User.find_by_id(3)).deliver_now
    # User.where(:is_login => true).find_each do |user|
    #   if user.token
    #     decoded_token = JsonWebToken.decode(user.token)
    #     if decoded_token
    #       puts Time.now.to_i > decoded_token[:exp]
    #     end
    #     # 
    #     # puts decoded_token.class
    #   end

    # end
    # decoded_token = JsonWebToken.decode('eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo2LCJleHAiOjE1MTEzNjY1MTJ9.qzPITxVlTKMP0V0tSlXgma-AEelij4XoWnTGEOpDzxY')
    return render json: JsonWebToken.decode('eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo3LCJleHAiOjE1MTEzNzAzNTV9.vHHOyBYD2gsWNhQzl-bKMNYK4YuRCdUkEaK-MvqQSis')
    # payload = {}
    # payload[:exp] = 1.minutes.from_now.to_i
    
    # render json: { token: JsonWebToken.decode('eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MTEzNjczNjV9.Jm1nL03Tt2I8xFqU0LG0-DKMOF5_mtXzhT3xVggRj4w')}
  end
  
  private

  def staff_statistic(user)
    @count_staff = UsersStaffRelation.where(:user_id => user.id).count
    receptions = Reception.where(:staff_id => user.id)

    @count_receptions = receptions.count
    @receptions_by_status = receptions.group(:status).count
    @receptions_by_date = receptions.where("receptions.start_date BETWEEN ? AND ?", params[:start_date], params[:end_date]).group("date(start_date)").count
    return {
      count_staff: @count_staff,
      receptions_by_status: @receptions_by_status,
      receptions_by_date: @receptions_by_date
    }
  end

  def admin_statistic(user)
    company_users = user.company.user
    @count_staff = company_users.where(:user_type => 'staffmember').count
    @count_clients = company_users.where(:user_type => 'user').count

    @reception_count = 0
    @receptions_by_status = {}
    @receptions_by_date = {}
    @payments_sum = 0

    company_users.where(:user_type => 'user').find_each do |user|
      @reception_count += user.reception.count

      user_receptions = Reception.where(:user_id => user.id)

      fill_hash(user_receptions.group(:status).count, @receptions_by_status)
      fill_hash(user_receptions.where("receptions.start_date BETWEEN ? AND ?", params[:start_date], params[:end_date]).group("date(start_date)").count, @receptions_by_date)

      user_receptions.find_each do |reception|
        @payment_history = reception.payment_history
        if @payment_history
          @payments_sum += @payment_history.amount
        end
      end
    end

    return {
      count_staff: @count_staff,
      count_clients: @count_clients,
      reception_count: @reception_count,
      receptions_by_status: @receptions_by_status,
      receptions_by_date: @receptions_by_date,
      payments_sum: @payments_sum
    }
  end

  def superadmin_statistic(user)
    @companies_count = Company.all.count
    # @payments = Payment.all
    @payments = Payment.where("payments.created_at BETWEEN ? AND ?", params[:start_date], params[:end_date])

    @payment_chart = @payments.group("date(created_at)").sum("amount")

    @income = 0;
    @payments.each do |payment|
      @income += payment.amount
    end
    return {
      companies_count: @companies_count,
      companies_approve: Company.where(:status => 'Approve').count,
      companies_rejected: Company.where(:status => 'Rejected').count,
      companies_new: Company.where("status IS NULL").count,
      income: @income,
      income_chart: @payment_chart
    }
  end

  def fill_hash(filled_hash, hash_to_fill)
    filled_hash.each do |key, value|
      if hash_to_fill.has_key?(key)
        hash_to_fill[key] += value
      else
        hash_to_fill[key] = value
      end
    end
  end

  def price_params
   params.require(:price).permit(:user_id, :amount)
  end

 

  private

  def working_hours_params
   params.require(:user).permit(:day, :start_time, :end_time)
  end

  def break_time_params
   params.require(:user).permit(:start_day, :end_day, :end_repeat, :repeat_type)
  end

  private

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :user_type, :is_verified, :is_active, :password, :logo)
  end

  private

  def change_pass_params
    params.require(:user).permit(:password)
  end

  private

  def user_update_params
    params.require(:user).permit(:first_name, :last_name, :email, :is_verified, :is_active, :logo)
  end

end