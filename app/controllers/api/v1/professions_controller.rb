class Api::V1::ProfessionsController < ApplicationController
  before_action :set_profession, only: [:show, :edit, :update, :destroy]
  before_action :set_company

  # GET /professions
  def index
    @professions = @company.profession.as_json(methods: :count_staff)
    render json: @professions
  end

  # GET /professions/1
  def show
    render json: @profession
  end

  # GET /professions/1/edit
  def edit
  end

  # POST /professions
  def create
    @profession = Profession.new({ title: profession_params[:title], company: @company })

    if @profession.save
      return render json: { profession: @profession, message: 'Profession was successfully created.' }
    else
      return render json: { errors: @profession.errors, message: 'Profession was not created.' }, status: 500
    end
  end

  # PATCH/PUT /professions/1
  def update
    if @profession.update(profession_params)
      return render json: { profession: @profession, message: 'Profession was successfully updated.' }
    else
      return render json: { errors: @profession.errors, message: 'Profession was not updated.' }, status: 500
    end
  end

  # DELETE /professions/1
  def destroy
    staff = @profession.user
    for user in staff
      _rel = ProfessionUser.where(profession_id: @profession.id, user_id: user.id).take
      _rel.destroy if _rel
    end
    @profession.destroy
    return render json: { profession: @profession, message: 'Profession was successfully destroyed.' }
  end

  def get_users
    if !@profession
      @profession = Profession.find(params[:id])
    end
    return render json: @profession.user
  end

  def add_user_to_profession
    @profession = Profession.find(params[:id])
    if !@profession
      return render json: { errors: ["Profession not found"] }, status: 404
    end
    @user = User.find(params[:user_id])
    if !@user
      return render json: { errors: ["User not found"] }, status: 404
    end
    @profession_user = ProfessionUser.where(user_id: params[:user_id]).take
    if @profession_user
      if @profession_user[:profession_id] != @profession[:id]
        @profession_user.update({ profession: @profession })
        return render json: { profession_user: @profession_user, message: 'User successfully added to profession' }
      else
        return render json: { profession_user: @profession_user, message: 'User already added to profession' }
      end
      # return render json: { profession_user: @profession_user, message: 'User already added to profession' }
    end
    @profession_user = ProfessionUser.new({ profession: @profession, user: @user })
    if @profession_user.save
      return render json: { profession_user: @profession_user, message: 'User successfully added to profession' }
    else
      return render json: { errors: @profession_user.errors }, status: 500
    end
  end

  def remove_user_from_profession
    @profession = Profession.find(params[:id])
    if !@profession
      return render json: { errors: ["Profession not found"] }, status: 404
    end
    @user = User.find(params[:user_id])
    if !@user
      return render json: { errors: ["User not found"] }, status: 404
    end
    @profession_user = ProfessionUser.where(user_id: params[:user_id]).take
    if @profession_user
      if @profession_user[:profession_id] == @profession[:id]
        @profession_user.delete
        return render json: { profession_user: @profession_user, message: 'User successfully removed from profession' }
      else
        return render json: { profession_user: @profession_user, message: 'User does not belong to the profession' }, status: 400
      end
    else
      return render json: { errors: ["User does not belong to the profession"] }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_profession
      @profession = Profession.find(params[:id])
    end

    def set_company
      @company = Company.find(params[:company_id])
    end

    # Only allow a trusted parameter "white list" through.
    def profession_params
      params.require(:profession).permit(:title)
    end
end
