class Api::V1::MenuItemsController < ApplicationController
	before_action :set_company

	def create
		@menu_items = params[:menu_items]
		items = Array.new
		for item in @menu_items
			company_menu_item = CompanyMenuItem.new(company: @company, menu_item_title: item[:menu_item_title], title: item[:title])
			if company_menu_item.save
				items << company_menu_item
			else
				return render json: company_menu_item.errors, status: 500
			end
		end
		return render json: items
	end

	def update
		company_menu_item = CompanyMenuItem.find_by_id(params[:menu_item_id])
		if company_menu_item.update(update_menu_item_params)
			return render json: company_menu_item
		else
			return render json: company_menu_item.errors, status: 500
		end
	end

	def get
		return render json: @company.company_menu_item
	end

	private
	def set_company
		@company = Company.find_by_id(params[:company_id])
	end

	def menu_item_params
		params.require(:menu_item).permit(:title, :menu_item_title)
	end

	def update_menu_item_params
		params.require(:menu_item).permit(:title, :menu_item_title)
	end
end
