AWS.config(
  :access_key_id => Rails.configuration.configData.content[:aws][:access_key_id],
  :secret_access_key => Rails.configuration.configData.content[:aws][:secret_access_key])
class Api::V1::PrescriptionsController < ApplicationController
  def get
    @prescription = Prescription.where(:reception_id => params[:reception_id]).take
    if @prescription
      render json: {prescription:@prescription, message:'success'}, status: 200
    else
      render json: { prescription: []}, status: 200
    end
  end
  def delete(id)
    @prescription = Prescription.find_by_id(id)
    if @prescription[:file_url]
          s3 = AWS::S3.new
          bucket = 'patientusers'
                b = s3.buckets[bucket]
                #delete S3 object 
                obj = b.objects[@prescription[:file_url]]
                obj.delete
    end
    if @prescription.destroy
      return {id:id,message:'success'}
    else
      return {id:id,message:'something wrong'}
    end
  end
  ## Update prescription
  def update
  	@prescription_data =  {
      is_approved: params[:is_approved],
    }
    if params[:message_body]
      @prescription_data[:message_body] = params[:message_body]
    end
    if params[:pharmacy_email]
      email = valid_email?(params[:pharmacy_email])
      if !email
          render json:  {errors: { pharmacy_email: "Email is not valid"}}, status: 200
          return
      end
      @prescription_data[:pharmacy_email] = params[:pharmacy_email]
    end
    @prescription = Prescription.where(:reception_id => params[:reception_id]).take
    @update_prescription = @prescription.update(@prescription_data)
      if @update_prescription
          if params[:is_approved]=='1'
            reception = Reception.find_by_id(params[:reception_id])
            patient = User.find_by_id(reception[:user_id])

            @prescription_email = {
               user_type:'user',
               user_email:patient[:email],
               reception_id:params[:reception_id]
            }
            PrescriptionMailer.PrescriptionMailer(@prescription_email).deliver_now 
          end
          render json: {prescription:@update_prescription,message:'success'}, status: 200
      else
        render json: { errors: {message:'somthing went wrong'}}, status: 500
      end
  end

  def create
    # email = valid_email?(params[:pharmacy_email])
    # if !email
    #       render json:  {errors: { pharmacy_email: "Email is not valid"}}, status: 200
    #       return
    # end

    if params[:prescription_id]
       delete_prescription = delete(params[:prescription_id])
    end

    file = params['file']
    @prescription =  {
      message_body: params[:message_body],
      sender_email: params[:sender_email],
      user_id:params[:user_id],
      reception_id:params[:reception_id]
    }

    if params[:pharmacy_email]
      email = valid_email?(params[:pharmacy_email])
      if !email
          render json:  {errors: { pharmacy_email: "Email is not valid"}}, status: 200
          return
      else
        @mail = PrescriptionMailer.PrescriptionMailer(@prescription).deliver_now 
        @prescription[:pharmacy_email] = params[:pharmacy_email]
      end
    end

    if file
      if file.path

        @prescription[:file_path] = file.path
        @prescription[:file_name] = File.basename file.path

        key = @prescription[:user_id]+'/'+@prescription[:file_name]
        new_file = @prescription[:file_path]
        upload = upload_to_s3(key,new_file)
        if upload
          @prescription_data =  {
            message_body: @prescription[:message_body],
            pharmacy_email: @prescription[:pharmacy_email],
            user_id:@prescription[:user_id],
            reception_id:@prescription[:reception_id],
            file_url:key
          }
          @new_prescription = Prescription.new(@prescription_data)

          if @new_prescription.save 
            render json: { prescription: @new_prescription}, status: 200
          else
            render json: { errors:@new_prescription.errors}, status: 200
          end
           
        else
          render json: { errors: {prescription:'File have not been upload to Amazon'}}, status: 200
        end
      else
          render json: { prescription: @prescription}, status: 200
      end
    else
      @prescription_data =  {
        message_body: @prescription[:message_body],
        pharmacy_email: @prescription[:pharmacy_email],
        user_id:@prescription[:user_id],
        reception_id:@prescription[:reception_id]
      }
      @new_prescription = Prescription.new(@prescription_data)
          if @new_prescription.save 
            render json: { prescription: @new_prescription}, status: 200
          else
            render json: { errors:@new_prescription.errors}, status: 200
          end
    end
  end

  def upload_to_s3(key,file)
     if key != ''
        s3 = AWS::S3.new
        bucket = 'patientusers'
              b = s3.buckets[bucket]
              if file
                  #create S3 object and save to current bucket
                          obj = b.objects[key].write(:file => file)
                          obj = obj.url_for(:read)
                  # add read permission to created object(file)
                  if obj.path
                        permission = b.objects[key]
                        permission.acl = :public_read
                  end
                        return key
              end
    else
        return ''
    end    
  end

  private
  def valid_email?(email)
    valid_email = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    return email.present? &&(email =~ valid_email)
  end


end

