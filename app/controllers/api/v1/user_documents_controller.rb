class Api::V1::UserDocumentsController < ApplicationController
  before_action :set_user_document, only: [:show, :edit, :update, :destroy]

  # GET /user_documents
  def index
    @user_documents = UserDocument.where(user_id: params[:id])
    render json: @user_documents
  end

  # GET /user_documents/1
  def show
    if (self.content_types.include? @user_document.content_type)
      render json: { user_document: @user_document }
    else
      send_data(@user_document.file_contents,
            type: @user_document.content_type,
            filename: @user_document.filename)
    end
   
  end

  # GET /user_documents/new
  def new
    @user_document = UserDocument.new
  end

  # GET /user_documents/1/edit
  def edit
  end

  # POST /user_documents
  def create
    file = params[:file]
    # render json: {content_type: file.content_type}
    # return

    if self.content_types.include? file.content_type
      self.save_image_document
    else
      @user_document = UserDocument.new({
        user_id: params[:id],
        document_id: params[:document_id],
        filename: file.original_filename,
        content_type: file.content_type,
        file_contents: file.read
      })
    end

    if @user_document.save
      render json: { user_document: @user_document, message: 'User document was successfully created.'}
    else
      render :new
    end
  end

  # PATCH/PUT /user_documents/1
  def update
    if @user_document.update(user_document_params)
      render json: { user_document: @user_document, message: 'User document was successfully updated.'}
    else
      render :edit
    end
  end

  # DELETE /user_documents/1
  def destroy
    @user_document.destroy
    render json: { message: 'User document was successfully destroyed.'}
  end

  def save_image_document()
    file = params[:file]
    @company_user_relation = CompanyUser.where(user_id: params[:id]).take
    if check_taken_space(@company_user_relation.company_id, file.size)
      s3 = AWS::S3.new
      bucket = 'patientusers'
      b = s3.buckets[bucket]
      filename = Time.now.to_i.to_s + file.original_filename
      key = "documents/" + filename
      #create S3 object and save to current bucket
      obj = b.objects[key].write(:file => file.path)
      obj = obj.url_for(:read)
      if obj.path
          permission = b.objects[key]
          permission.acl = :public_read
      end

      @user_document = UserDocument.new({
          user_id: params[:id],
          document_id: params[:document_id],
          filename: filename,
          content_type: file.content_type,
          file_contents: "http://" + obj.host + obj.path
        })
    else
      render json: { errors: "There are no enough space"}, status: 500
    end
  end

  def content_types
    ['image/gif', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/svg+xml', 'image/tiff', 'image/vnd.microsoft.icon', 'image/vnd.wap.wbmp', 'image/webp']
  end

  private
    

    # Use callbacks to share common setup or constraints between actions.
    def set_user_document
      @user_document = UserDocument.find(params[:user_document_id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_document_params
      params.require(:user_document).permit(:user_id, :document_id, :filename, :content_type, :file_contents)
    end
end
