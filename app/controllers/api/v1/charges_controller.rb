class Api::V1::ChargesController < ApplicationController

	def create
		  # Amount in cents

		@user = User.find_by_id(params[:user_id])
		@superadmin = User.where(user_type: "superadmin").take
		@admin_stripe_key = SripeCredential.where(user_id: @superadmin.id).take
		if !@admin_stripe_key
			render json: {errors: {key:'Your payment have been failed, because current admin have not any stripe account'}}, status: 200
			return
		end

		# Stripe.api_key ='sk_test_qXK8nGDCFDJimbie6LfskAkj'
		Stripe.api_key = @admin_stripe_key[:key]

		if @user
			stripeToken = Stripe::Token.create(
			    :card => {
			      :number => params[:number],
			      :exp_month => params[:exp_month],
			      :exp_year => params[:exp_year],
			      :cvc => params[:cvc]
			    }
			)

			customer = Stripe::Customer.create(
			    :email => params[:email],
			    :source  => stripeToken.id
			)

			charge = Stripe::Charge.create(
			    :customer    => customer.id,
			    :amount      => params[:amount],
			    :description => 'Rails Stripe customer',
			    :currency    => 'usd'
			)

			@card = CreditCard.where(number: params[:number]).take
			if !@card
				@card = CreditCard.new(
	                :number             => params[:number],
	                :card_type          => stripeToken.card.brand,
	                :month              => params[:exp_month],
	                :year               => params[:exp_year],
	                :cvc => params[:cvc]
	                )
				@card.save
				
			end

			@u_c = UserCreditCard.where(user_id: @user.id, credit_card_id: @card.id).take
			
			if !@u_c
				@u_c = UserCreditCard.new(:user_id => @user.id, :credit_card_id => @card.id)
				@u_c.save
			end

			@payment = Payment.new(
				:user_id => @user.id, 
				:credit_card => @card, 
				:plan_id => params[:plan_id],
				:amount => params[:amount], 
				:description => params[:description])

			@payment.save

			UserMailer.successfully_charge(@user, params[:amount]).deliver_later


			render json: { message: "Successfully charged" }

		else
			render json: { message: "User not found" }, status: 500
		end

		

		rescue Stripe::CardError => e
		  flash[:error] = e.message
		  render json: { errors: e.message }, status: 500
	end


	def show
		@payments  = Payment.all
		render json: @payments
	end

	def export
		@payments  = Payment.all
		@csv_data = CSV.generate do |csv|
		  csv << ["id","company_id", "company_name", "company_email", "admin_id", "admin_email", "plan_id", "amount", "description", "created_at", "updated_at"]
		  @payments.each do |payment|
		  	csv << [payment.id, payment.company.id, payment.company.name, payment.company.email, payment.user.id, payment.user.email, payment.plan_id, payment.amount, payment.description, payment.created_at, payment.updated_at]
		  end
		end
		render json: { data: @csv_data }
	end

end
