class Api::V1::PlanController < ApplicationController

	def create
		user = User.find_by_id(params[:id])
		if user
			plan = Plan.new(plan_params)
			if !plan.text_color
				plan.text_color = '#ffffff'
			end
			if plan.save
				user_plan = UserPlan.new({
						user_id: params[:id],
						plan_id: plan.id
					})

				if params[:plan][:feature]
					for feature in params[:plan][:feature]
						_feature = Feature.new({plan: plan, title: feature[:title] })
						_feature.save
					end
				end
				if user_plan.save
					render json: { plan: plan }, status: 200
				else
					render json: { errors: user_plan.errors}, status: 500
				end
			else 
				render json: { errors: plan.errors}, status: 500
			end
		else
			render json: { errors: "User not found" }, status: 404
		end
	end

	def update
		user = User.find_by_id(params[:id])

		if user
			plan = Plan.find_by_id(params[:plan_id])
	      	if plan
		        if plan.update(plan_params)
		            render json: { plan: plan }, status: 200
		        else
		          	render json: { errors: plan.errors }, status: 200
		        end
	      	else
	       		render json: { errors: "User plan not found" }, status: 200
	      	end
	    else
	    	render json: { errors: "User not found" }, status: 404
	    end
	end

	def delete
		begin
			user = User.find_by_id(params[:id])
			if user
			    plan = Plan.find_by_id(params[:plan_id])
			    if plan

			    	rel = UserPlan.where(user_id: params[:id], plan_id: params[:plan_id]).take;
					rel.destroy if rel

				    if plan.destroy
				        render json: { success: "Plan successfully deleted" }
				    else
				        render json: { errors: "Plan not deleted" }, status: 500
				    end
			    else
			      render json: { errors: "Plan not found" }, status: 404
			    end
			else
				render json: { errors: "User not found" }, status: 404
			end
		rescue ActiveRecord::InvalidForeignKey
			render json: { errors: "Plan registered by company" }, status: 500
		end
	end

	def plan
		# user = User.find_by_id(params[:id])
		# if user
			plan = Plan.find_by_id(params[:plan_id])
			if plan
				render json: { plan: plan }
			else
				render json: { message: "Plan not found" }, status: 404
			end
		# else
		# 	render json: { errors: "User not found" }, status: 404
		# end
	end

	def plans
		@plans = Plan.all

		plans = Array.new
		for @plan in @plans
			plan = @plan.serializable_hash
			features = @plan.features
			plan[:feature] = features
			plans << plan
		end

	    render json: { plans: plans }
	end

	def plan_params
		params.require(:plan).permit(:title, :price, :description, :plan_type, :color, :text_color, :is_logo_allowed, :is_color_allowed, :staff_members, :memory)
	end
end
