class Api::V1::TwocheckoutController < ApplicationController
	def create
		@superadmin = User.where(user_type: "superadmin").take
		if @superadmin
			@credentials = TwocheckoutCredential.where(user_id: @superadmin.id).take
			if @credentials
				Twocheckout::API.credentials = {
				    :seller_id => @credentials.seller_id,
				    :private_key => @credentials.private_key,
				    :sandbox => 1
				}

				@payment = {
					:merchantOrderId=> Time.now.to_i,
				    :token          => params[:token],
				    :currency       => 'USD',
				    :total          => params[:amount],
				    :billingAddr    => {
				        :name => params[:billing_address][:name],
				        :addrLine1 => params[:billing_address][:addressLine1],
				        :city => params[:billing_address][:city],
				        :state => params[:billing_address][:state],
				        :zipCode => params[:billing_address][:zipCode],
				        :country => params[:billing_address][:country],
				        :email => params[:billing_address][:email],
				        :phoneNumber => params[:billing_address][:phone]
				    }
				}

				begin
					result = Twocheckout::Checkout.authorize(@payment)
					@payment = Payment.new(
						:user_id => params[:user_id],
						:plan_id => params[:plan_id],
						:amount => params[:amount])

					@payment.save
					render json: {output: result}, status: 200
				rescue Twocheckout::TwocheckoutError => e
					render json: {errors: e.message}, status: 500
				rescue Exception => e  
					render json: {errors: e.message}, status: 500
				end
			end
		end
	end

	def pay
		@admin_relation = UsersStaffRelation.where(staff_id: payment_params[:staff_id]).take
		@admin_id = @admin_relation[:user_id]

		@admin = User.find_by_id(@admin_id)
		@seller_email = @admin[:email]
		@company = Company.where(user_id: @admin.id).take

		@patient = User.find_by_id(payment_params[:patient_id])
		@customer_email = @patient[:email]

		@credentials = TwocheckoutCredential.where(user_id: @admin_id).take
		if !@credentials
			render json: {errors: {key:'Sorry, your Payment has not been processed because this buisness does not use a 2Checkout Account. Please try again with a Credit Card Payment.'}}, status: 200
			return
		end

		Twocheckout::API.credentials = {
		    :seller_id => @credentials.seller_id,
		    :private_key => @credentials.private_key,
		    :sandbox => 1
		}

		@payment = {
			:merchantOrderId=> Time.now.to_i,
		    :token          => payment_params[:token],
		    :currency       => 'USD',
		    :total          => payment_params[:amount],
		    :billingAddr    => {
		        :name => params[:billing_address][:name],
		        :addrLine1 => params[:billing_address][:addressLine1],
		        :city => params[:billing_address][:city],
		        :state => params[:billing_address][:state],
		        :zipCode => params[:billing_address][:zipCode],
		        :country => params[:billing_address][:country],
		        :email => params[:billing_address][:email],
		        :phoneNumber => params[:billing_address][:phone]
		    }
		}

		begin
			result = Twocheckout::Checkout.authorize(@payment)
			@payment = {
				seller_id: @admin[:id],
				customer_id: payment_params[:patient_id],
				amount: payment_params[:amount],
				reception_id: payment_params[:reception_id],
				status: 'Paid'
			}
			@payment_history = PaymentHistory.new(@payment)
			if @payment_history.save
				render json: {payment_history: {payment:@payment,message:'Your payment have been successful'}}, status: 200
			else 
				render json: {errors: 'Something went wrong'}, status: 200
			end
			# render json: { output: "Order Complete: #{result}"}
		rescue Twocheckout::TwocheckoutError => e
			"Order Failed: #{e.message}"
		end
	end

	private
		def payment_params
			params.require(:payment).permit(:reception_id, :amount ,:staff_id ,:patient_id, :token)
		end
end
