class Api::V1::PaypalController < ApplicationController
	def create
		@superadmin = User.where(user_type: "superadmin").take
		if @superadmin
			@credentials = PaypalCredential.where(user_id: @superadmin.id).take
			if @credentials
				@plan = Plan.find_by_id(params[:plan_id])
				if @plan
					PayPal::SDK::REST.set_config(
						:mode => "sandbox", # "sandbox" or "live"
						:client_id => @credentials.client_id,
						:client_secret => @credentials.client_secret)

					@payment_params = {
						id: params[:user_id],
						plan_id: @plan.id,
						amount: @plan.price,
						description: @plan.description
					}
					@payment = PayPal::SDK::REST::DataTypes::Payment.new({
						:intent => "sale",
						:payer => {
							:payment_method => "paypal" 
						},
						:transactions => [{
							:item_list => {
								:items => [{
									:name => @plan.title,
									:sku => @plan.id,
									:price => @plan.price,
									:currency => "USD",
									:quantity => 1 
								}]
							},
							:amount => {
								:total => @plan.price,
								:currency => "USD"
							},
							:description => @plan.description
						}],
						:redirect_urls => {
							:return_url => "http://localhost:3000/api/v1/paypal/save_admin" + to_url_params(@payment_params),
							:cancel_url => "http://localhost:3000/api/v1/paypal/save_admin"
						}
					})
					if @payment.create
						render json: {link: @payment.links.find{|v| v.rel == "approval_url" }.href}, status: 200
					else
						render json: {errors: @payment.error}, status: 500
					end
				else
					render json: {errors: "You should choose a plan."}, status: 500
				end
			else
				render json: {errors: "There are no connected paypal credentials."}, status: 500
			end
		else
			render json: {errors: "Superadmin is not registered yet."}, status: 500
		end
	end

	def pay
		@admin_relation = UsersStaffRelation.where(staff_id: payment_params[:staff_id]).take
		@admin_id = @admin_relation[:user_id]

		@admin = User.find_by_id(@admin_id)
		@seller_email = @admin[:email]
		@company = Company.where(user_id: @admin.id).take

		@patient = User.find_by_id(payment_params[:patient_id])
		@customer_email = @patient[:email]

		@credentials = PaypalCredential.where(user_id: @admin_id).take
		if !@credentials
			render json: {errors: {key:'Sorry, your Payment has not been processed because this buisness does not use a PayPal Account. Please try again with a Credit Card Payment.'}}, status: 200
			return
		end

		PayPal::SDK::REST.set_config(
			:mode => "sandbox", # "sandbox" or "live"
			:client_id => @credentials.client_id,
			:client_secret => @credentials.client_secret)

		@payment_params = {
			admin_id: @admin_id,
			patient_id: payment_params[:patient_id],
			amount: payment_params[:amount],
			reception_id: payment_params[:reception_id]
		}
		@payment = PayPal::SDK::REST::DataTypes::Payment.new({
			:intent => "sale",
			:payer => {
				:payment_method => "paypal" 
			},
			:transactions => [{
				:item_list => {
					:items => [{
						:name => "Reception payment",
						:sku => payment_params[:reception_id],
						:price => payment_params[:amount],
						:currency => "USD",
						:quantity => 1 
					}]
				},
				:amount => {
					:total => payment_params[:amount],
					:currency => "USD"
				},
				:description => ''
			}],
			:redirect_urls => {
				:return_url => "http://localhost:3000/api/v1/paypal/save" + to_url_params(@payment_params),
				:cancel_url => "http://localhost:3000/api/v1/paypal/save"
			}
		})
		if @payment.create
			render json: {link: @payment.links.find{|v| v.rel == "approval_url" }.href}, status: 200
		else
			render json: {errors: @payment.error}, status: 500
		end
	end

	def save_payment
		@company = Company.where(user_id: params[:admin_id]).take
		if params[:paymentId]
			@payment = {
					seller_id: params[:admin_id],
					customer_id: params[:patient_id],
					amount: params[:amount],
					reception_id: params[:reception_id],
					status: 'Paid'
				}
			@payment_history = PaymentHistory.new(@payment)
			@payment_history.save
		end
		redirect_to "http://" + @company.subdomain + ".localhost:8000/#!/user?view_receptions_by=list&filter_receptions_by=upcoming"
	end

	def save_admin_payment
		if params[:paymentId]
			@payment = Payment.new(
				:user_id => params[:id],
				:plan_id => params[:plan_id],
				:amount => params[:amount], 
				:description => params[:description])
			@payment.save
			redirect_to "http://localhost:8000/#!/admin_register?paid=true"
		else
			redirect_to "http://localhost:8000/#!/admin_register?cancelled=true"
		end
	end

	private
		def payment_params
			params.require(:payment).permit(:reception_id, :amount ,:staff_id ,:patient_id )
		end

		def to_url_params(obj)
			url_params = ''
			obj.each do |key, value|
				if url_params.empty?
					url_params += "?" + key.to_s + "=" + value.to_s
				else
					url_params += "&" + key.to_s + "=" + value.to_s
				end
			end
			url_params
		end 
end