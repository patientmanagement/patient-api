class Api::V1::StripeController < ApplicationController

def new
end

def create
  credit_card = ActiveMerchant::Billing::CreditCard.new(
                  :first_name         => 'Holdname',
                  :last_name          => 'Holdname',
                  :number             => card_params[:number],
                  :brand              => card_params[:card_type],
                  :month              => card_params[:month],
                  :year               => card_params[:year],
                  :verification_value => card_params[:cvc]).validate
  if credit_card !={}
    render json: {errors:credit_card} , status:200
    return
  end
  # Amount in cents

    @amount = payment_params[:amount]
    # Get seller and customer emails
    @admin_relation = UsersStaffRelation.where(staff_id:payment_params[:staff_id]).take
    @admin_id = @admin_relation[:user_id]

  	@admin = User.find_by_id(@admin_id)
  	@seller_email = @admin[:email]

  	@patient = User.find_by_id(payment_params[:patient_id])
  	@customer_email = @patient[:email]

  


  @admin_stripe_key = SripeCredential.where(user_id: @admin_id).take
  if !@admin_stripe_key
    render json: {errors: {key:'Your payment have been failed, because current admin have not any stripe account'}}, status: 200
    return
  end

  # Stripe.api_key ='sk_test_qXK8nGDCFDJimbie6LfskAkj'
  Stripe.api_key = @admin_stripe_key[:key]
  stripeToken = Stripe::Token.create(
    :card => {
      :number => card_params[:number],
      :exp_month => card_params[:month],
      :exp_year => card_params[:year],
      :cvc => card_params[:cvc]
    }
  )

  customer = Stripe::Customer.create(
    :email => @customer_email,
    # :source  => payment_params[:stripeToken]
    :source  => stripeToken.id

  )

  charge = Stripe::Charge.create(
    :customer    => customer.id,
    :amount      => @amount,
    :description => 'Rails Stripe customer',
    :currency    => 'usd'
  )

  # e = Stripe::CardError
  # render json: {errors:{stripe:e}}, status: 200
  #         return
  # rescue Stripe::CardError => e
  
  #       if(e.message)
  #         render json: {errors:{stripe:e.message}}, status: 200
  #         return
  #       end

  if charge[:status] == 'succeeded'
  	# Send emails
  	@customer={
  		email:@patient[:email],
  		user_type:@patient[:user_type],
  		amount: charge[:amount]/100,
  		reception_id:payment_params[:reception_id],
  		first_name:@patient[:first_name],
  		last_name:@patient[:last_name],
      company: @patient.company
  		}
  	@seller={
  		email:@admin[:email],
  		user_type:@admin[:user_type],
  		amount: charge[:amount]/100,
  		reception_id:payment_params[:reception_id],
  		first_name:@admin[:first_name],
  		last_name:@admin[:last_name],
  		patient:@customer,
      company: @admin.company
  		}

  	StripeMailer.stripe_mailer(@seller, payment_params[:reception_id]).deliver_now 
  	StripeMailer.stripe_mailer(@customer, payment_params[:reception_id]).deliver_now  

  	@payment = {
  		seller_id: @admin[:id],
  		customer_id: payment_params[:patient_id],
  		amount: charge[:amount],
  		reception_id:payment_params[:reception_id],
  		status:charge[:status]
  	}
  	@payment_history = PaymentHistory.new(@payment)
  	if @payment_history.save
  	   render json: {payment_history: {payment:@payment,message:'Your payment have been successful'}}, status: 200
  	else 
  	   render json: {errors: 'Something went wrong'}, status: 200
  	end
  else
    render json: {errors: 'Your payment have  been failed'}, status: 200
  end
end

def all_payment_history
   @payment_histories = PaymentHistory.all
    render json: {payment_histories: @payment_histories}, status: 200
end 
def cleanup string
  string.titleize
end
def payment_history
   @payment_histories = PaymentHistory.where(seller_id:params[:id])
    @payments = Array.new
    @payment = {}
    for payment_history in @payment_histories
            @client = User.where(id:payment_history[:customer_id]).take
            @reception=Reception.where(id:payment_history[:reception_id]).take
          unless @reception.nil?
            @staffmember = User.where(id:@reception[:staff_id]).take


            @payment.store("info",payment_history)
            @payment.store("reception",@reception)
            @payment.store("client",@client)
            @payment.store("staffmember",@staffmember)

            @payments << @payment
            @payment= {}
          end
    end  
    render json: {payment_histories: @payments}, status: 200
end

def payment_history_delete
    @payment_history=PaymentHistory.find_by_id(params[:id])
    if @payment_history
      if @payment_history.destroy
        render json: { success: "Payment history successfully deleted" }
      else
        render json: { success: "Payment history have not successfully deleted" }
      end
    else
      render json: { success: "Payment history not found" }
    end
  end

private

  def payment_params
    params.require(:payment).permit(:reception_id, :stripeToken, :amount ,:staff_id ,:patient_id )
  end

private

  def card_params
    params.require(:card).permit(:number,:month,:year,:cvc)
  end
end
