class Api::V1::DocumentController < ApplicationController

	def create
		field = Field.find_by_id(params[:id])
		if field
			doc = Document.new(doc_params)

			if doc.save
				field_doc = FieldDocument.new({
						field_id: params[:id],
						document_id: doc.id
					})
				if field_doc.save
					render json: { document: doc }, status: 200
				else
					render json: { errors: field_doc.errors}, status: 500
				end
			else 
				render json: { errors: doc.errors}, status: 500
			end
		else
			render json: { errors: "Field not found" }, status: 404
		end
	end

	def update
		field = Field.find_by_id(params[:id])
		if field
			doc = Document.find_by_id(params[:doc_id])

			if doc.update(doc_params)
				render json: { document: doc }, status: 200
			else 
				render json: { errors: doc.errors}, status: 500
			end
		else
			render json: { errors: "Field not found" }, status: 404
		end
	end

	def delete
		field = Field.find_by_id(params[:id])
		if field

			doc = Document.find_by_id(params[:doc_id])
			if doc

				rel = FieldDocument.where(field_id: params[:id], document_id: params[:doc_id]).take;
				rel.destroy if rel

				if doc.destroy
					render json: { message: "Document deleted successfully" }, status: 200
				else 
					render json: { errors: "Document not deleted" }, status: 500
				end
			else
				render json: { errors: "Document not found" }, status: 404
			end
		else
			render json: { errors: "Field not found" }, status: 404
		end
	end

	def doc

		field = Field.find_by_id(params[:id])
		if field
			doc = Document.find_by_id(params[:doc_id])
			if doc
				render json: { document: doc }
			else
				render json: { errors: "Document not found" }, status: 404
			end
		else
			render json: { errors: "Field not found" }, status: 404
		end
	end

	def docs
		field_docs = FieldDocument.where(field_id: params[:id])

		docs = Array.new
		for relation in field_docs
	        doc = Document.find_by_id(relation.document_id)
	        docs << doc
	    end 

	    render json: docs
	end

	def doc_params
		params.require(:doc).permit(:title)
	end
end
