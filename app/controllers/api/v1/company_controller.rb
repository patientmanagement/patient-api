require 'securerandom'
require 'csv'
class Api::V1::CompanyController < ApplicationController
	def create
		company = Company.where(subdomain: company_params[:subdomain]).take
		if !correct_subdomain(company_params[:subdomain])
			return render json: { errors: { subdomain: ["Subdomain should consist of letters or numbers. Should start from a letter."] } }, status: 400
		end
		if company
			return render json: { errors: { subdomain: ["This subdomain is already taken"] } }, status: 400
		end
		company = Company.new(company_params)
		company.approve_key = SecureRandom.hex
	    if company.save

			company_user = CompanyUser.new(company_id: company.id, user_id: params[:company][:user_id])
			company_user.save
			@user = User.find_by_id(params[:company][:user_id])
			if @user
				UserMailer.registration_confirmation(@user, "").deliver_now 
			end

			render json: { company: company }, status: 200
	    else
			render json: { errors: company.errors}, status: 200
	    end
	end

	def update
		company = Company.find_by_id(params[:id])
		old_status = company.status
	    if company
	        if company.update(company_params)
	        	if old_status != company.status
	        		@user = User.find_by_id(company.user_id)
	        		if @user
	        			if company.status == 'Approve'
	        				@user.is_active = '1'
	        				CompanyMail.company_approved(@user).deliver_later
	        			else
	        				@user.is_active = '0'
	        				CompanyMail.company_rejected(@user).deliver_later
	        			end
	        			@user.save
	        		end
	        		# CompanyMail.send_company(@user, company.status).deliver_now
	        	end
	          render json: { company: company }, status: 200
	        else
	          render json: { errors: company.errors }, status: 500
	        end
	    else
	        render json: { errors: "Company not found" }, status: 404
	    end
	end

	def delete
		company = Company.find_by_id(params[:id])
	    if company
	        if company.destroy
	          render json: { message: "Company successfully deleted" }, status: 200
	        else
	          render json: { errors: company.errors }, status: 500
	        end
	    else
	        render json: { errors: "Company not found" }, status: 404
	    end
	end

	def company
		company = Company.find(params[:id])
	    if company
	    	# company.user.where(user_type: 'admin').take
	        render json: { company: company, admin: company.user.where(user_type: 'admin').take  }, status: 200
	    else
	        render json: { errors: "Company not found" }, status: 404
	    end
	end

	def get_user_company
		user = User.find(params[:id])
		if user
			if user.user_type == 'admin'
				company = Company.where(user_id: params[:id]).take
			else
				company = Company.joins(:company_user).where('company_users.user_id = ?', params[:id]).take
			end
		end

		if company
	        render json: { company: company }, status: 200
	    else
	        render json: { errors: "Company not found" }, status: 404
	    end
	end

	def companies
		if !params[:is_charged]

			companies = Company.all

			if params[:subdomain]
				companies = Company.where(subdomain: params[:subdomain]).take
			end

	    	render json: companies
		else
			company = Company.where(subdomain: params[:subdomain]).take
			unless company.nil?
				user_id = company.user_id
			else
				return render json: { errors: "No such company" }
			end
			last_payment = Payment.where(user_id: user_id).last
			if last_payment.nil?
				return render json: { errors: "Company didn't choose plan yet" }
			end
			plan = Plan.find(last_payment.plan_id)
			count_days = 0
			case plan.plan_type
			when 'weekly'
				count_days = 7
			when 'monthly'
				count_days = 30
			when 'yearly'
				count_days = 365
			end
			if (last_payment && (Date.today - (Time.parse(last_payment.created_at.to_s).to_date + count_days)).to_i < 0)
				render json: { charged: true }
			else
				render json: { charged: false }
			end
		end
	end

	def save_company_logo
        file = params[:file]
        if file
		    s3 = AWS::S3.new
		    bucket = 'patientusers'
		    b = s3.buckets[bucket]
		    filename = Time.now.to_i.to_s + file.original_filename
		    key = "companies/" + filename
		    #create S3 object and save to current bucket
		    obj = b.objects[key].write(:file => file.path)
		    obj = obj.url_for(:read)
		    if obj.path
		        permission = b.objects[key]
		        permission.acl = :public_read
		    end
		    url = "http://" + obj.host + obj.path
		    
		    if params[:company_id]
		    	company = Company.find_by_id(params[:company_id])
		    	if company
		    		company.logo = url
		    		company.save
		    	end
		    end

		    return render json: { url: url }
	    else
	    	return render json: { errors: "File not found" }, status: 500
	    end
	end

	def company_params
		params.require(:company).permit(:user_id, :name, :email, :address, :subdomain, :logo, :color, :status)
	end

	def check_taken_space
		@s3 = AWS::S3.new
		@bucket = @s3.buckets['patientusers']
		@size = 0
		@company = Company.find_by_id(params[:company_id])
		@bucket.objects.each do |object|
		  if @company.logo.include? object.key
		    @size += object.content_length
		  end
		  @company.user.each do |user|
		    if user.logo.include? object.key
		      @size += object.content_length
		    end
		    UserDocument.all.each do |document|
		      if document.user_id == user.id
		        if document.file_contents.include? object.key
		          @size += object.content_length
		        end
		      end
		    end
		  end
		end
		render json: {size: (@size / (1024.0 * 1024.0 * 1024.0)).to_s + ' GB'}
	end

	def export
		@companies = Company.all
		@csv_data = CSV.generate do |csv|
		  csv << ["user_id", "name", "email", "address", "subdomain", "logo", "color", "status", "approve_key", "created_at", "updated_at"]
		  @companies.each do |company|
		  	csv << [company.user_id, company.name, company.email, company.address, company.subdomain, company.logo, company.color, company.status, company.approve_key, company.created_at, company.updated_at]
		  end
		end
		render json: { data: @csv_data }
	end

	def correct_subdomain(subdomain)
		!subdomain.match(/^[a-zA-Z][a-zA-Z0-9]+$/).nil?
	end
end
