# require 'net/http'
# require "openssl"
class Api::V1::VideoChatController < ApplicationController

  def create
  	@video_chat=VideoChat.new(video_chat_params)
    if @video_chat.save
      @user_video_chat_rell = UserVideoChatRelation.new({user_id: params[:user_id], video_chat_id: @video_chat.id})
      @user_video_chat_rell.save
      render json: {video_chat: @video_chat, user_video_chat_rell: @user_video_chat_rell}, status: 200
    else
      render json: { errors: @video_chat.errors}, status: 200
    end	
  end

  def update
  	@video_chat=VideoChat.where(video_chat_id: params[:id]).take
    if @video_chat.update({end_video_chat: video_chat_params[:end_video_chat], is_active: video_chat_params[:is_active]})
      render json: @video_chat, status: 200
    else
      render json: { errors: @video_chat.errors}, status: 200
    end	
  end

  def get_video_chat
    @video_chat=VideoChat.where(video_chat_id: params[:session_id]).take
    if @video_chat
      render json: @video_chat, status: 200
    else
      render json: { errors: 'Video Chat not found.'}, status: 200
    end 
  end

  def all_video_chat
    @user_video_chat_rell=UserVideoChatRelation.where(user_id: params[:user_id])
    @video_chat_inf = Array.new
      for relation in @user_video_chat_rell
        @video_chat=VideoChat.find_by_id(relation.video_chat_id)
        if @video_chat
           @video_chat_inf << @video_chat
        end
      end
      render json: @video_chat_inf , status: 200
  end

  def delete
    @user_video_chat_rell=UserVideoChatRelation.where(user_id: params[:user_id], video_chat_id: params[:video_chat_id])
   
    render json: @user_video_chat_rell
    # if @user_video_chat_rell.count != 0
    #   @video_chat=VideoChat.where(id: @user_video_chat_rell.video_chat_id).take
    #   for relation in @user_video_chat_rell
    #     @video_chat=VideoChat.where(id: relation.video_chat_id).take
    #     if @video_chat
    #       @video_chat.destroy
    #       relation.destroy
    #       render json: { success: "Video Chat successfully deleted" }
    #     else
    #       render json: { errors: "Video Chat not found" }
    #     end
    #   end
    # else
    #   render json: { errors: "Video Chat not found" }
    # end
  end

 
  def create_user_chat_rell
    @video_chat = VideoChat.where(video_chat_id: params[:video_chat_id]).take
    if @video_chat && params[:user_id]
          @user_video_chat_rell = UserVideoChatRelation.new({user_id: params[:user_id], video_chat_id: @video_chat.id})
          @user_video_chat_rell.save
          render json: { success: "User Video Chat Relation successfully deleted" }
    else
      render json: { errors: "Video Chat not found" }
    end
  end


  def start_twiddla

    uri = URI('https://www.twiddla.com/API/')
    http = Net::HTTP.new(uri.host, uri.port)
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    http.use_ssl = true
    request = Net::HTTP::Post.new(uri.path + 'CreateMeeting.aspx', {'Content-Type' => 'application/json'})
    request.set_form_data(params)
    response = http.request(request)
    puts response.body
    begin
      session_id = Integer(response.body)
    rescue ArgumentError
      return render json: { error: response.body }, status: 400
    end
    msg = {
      session_id: session_id,
      room: params[:room],
      clients_id: params[:clients_id]
    }
    $redis.publish 'whiteboard-session-created', msg.to_json
    
    return render json: { session_id: session_id }
  end

  def whiteboard

    $redis.publish params[:channel], params[:data].to_json

    render json: { status: 'OK' }
  end

  def get_video_chat_messages
    video_chat=VideoChat.find_by_id(params[:id])
    render json: video_chat.video_chat_message
  end

  def add_message
    
    video_chat=VideoChat.where(video_chat_id: video_chat_message_params[:video_chat_id]).take
    if video_chat
      video_chat_message = VideoChatMessage.new(:video_chat_id => video_chat.id, :user_id => video_chat_message_params[:user_id], :message => video_chat_message_params[:message])
      video_chat_message.save
    end
    render json: video_chat_message
  end

  def video_chat_params
    params.require(:video_chat).permit(:video_chat_id, :video_chat_name, :is_active, :start_video_chat, :end_video_chat)
  end

  def video_chat_message_params
    params.require(:video_chat_message).permit(:video_chat_id, :user_id, :message)
  end
end
