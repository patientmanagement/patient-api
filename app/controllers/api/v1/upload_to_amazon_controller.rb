AWS.config(
  :access_key_id => 'AKIAJG3FAZ6DBPS37FNQ',
  :secret_access_key => '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')
class Api::V1::UploadToAmazonController < ApplicationController
  protect_from_forgery
  respond_to :json
  
  def getVideos  
      user_folder = params['user_id']
      s3 = AWS::S3.new
      bucket = 'newbucketforvideo'
      prefix = user_folder + '/videos'
          b = s3.buckets[bucket]
          #get array of objects keys
          collection = b.objects.with_prefix(prefix).collect(&:key)
      render json: {collection:collection}, status: 200
  end
  
  def save_file_to_s3
     key = params['key']
     folder = params['folder']
     if key != '' && folder !=''
      s3 = AWS::S3.new
        bucket = 'newbucketforvideo'
        key = folder+'/videos/'+key
              b = s3.buckets[bucket]
              file = params['file']
              if file.path
                  path_to_file = file.path
                  file_size = file.size
                  file_exp = file.eof?
                  #create S3 object and save to current bucket
                          obj = b.objects[key].write(:file => path_to_file)
                          obj = obj.url_for(:read)
                  # add read permission to created object(file)
                  if obj.path
                        permission = b.objects[key]
                        permission.acl = :public_read
                  end
                  render json: {bucket:bucket,path:obj.path,obj:obj,key:key,exp:file_exp}, status: 200
              else 
                        render json: {params:params,path:'',obj:'',key:key,message:'empty file content'}, status: 200
              end

    else
        render json: {params:params,path:'',obj:'',key:key,message:'empty file name'}, status: 200
    end    
  end

  def remove_file_from_s3
      key = params['key'] 
       if key != ''
        s3 = AWS::S3.new
          bucket = 'newbucketforvideo'
                b = s3.buckets[bucket]
                #delete S3 object 
                obj = b.objects[key]
                obj.delete

          render json: {params:params}, status: 201
      else   
         render json: {params:params,message:'empty file name'}, status: 200
      end
  end

  def remove_folder_from_s3
      user_folder = params['user_id'] 
       if user_folder != ''
        s3 = AWS::S3.new
          bucket = 'newbucketforvideo'
          folder = 'videos/'+user_folder
                b = s3.buckets[bucket]
                #delete all files in folder
                obj_in_folder = b.objects.with_prefix(folder).collect(&:key)
                obj_in_folder.each do |key|
                   obj = b.objects[key]
                   obj.delete
                end

                #delete S3 folder 
                obj = b.objects[folder]
                obj.delete

          render json: {params:params}, status: 201
      else   
         render json: {params:params,message:'empty file name'}, status: 200
      end
  end

  def delete_test
  render json: {params:'params',message:'delete method'}, status: 200
  end

end
