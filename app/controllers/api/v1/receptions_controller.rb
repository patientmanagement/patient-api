class Date
  def self.parsable?(string)
    begin
      parse(string)
      true
    rescue ArgumentError
      false
    end
  end
end
AWS.config(
  :access_key_id => 'AKIAJG3FAZ6DBPS37FNQ',
  :secret_access_key => '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')
class Api::V1::ReceptionsController < ApplicationController
  # Creating reception
  def message 
     @data = Rails.configuration.configData.content
     return @data[:message][:receptions]
     #render json: {message:@data[:message][:controller][:receptions]}, status: 200
  end
  def create
    if reception_params[:is_success]
      @reception=Reception.new(reception_params[:param])
      if @reception.save 
          @reception_message_ob = reception_message
          @reception_message_ob.store("reception_id",@reception.id)
          @reception_message=ReceptionMessage.new(@reception_message_ob)
          if @reception_message.save 
            @reception_message = "new message has created successfully"
          else
            @reception_message = "server_error"
          end
        render json: {reception:@reception,message:{reception: message[:create_succes],info_message:reception_message}}, status: 200
      else
        render json: { errors: message[:server_error]}, status: 201
      end
    else
        render json: { errors: reception_params[:message]}, status: 201
    end
  end
  ## Update reception
  def update
    @reception = Reception.find_by_id(params[:id])
    if @reception

      if update_reception_params[:is_success]
        old_reception_status = @reception.status;
        if @reception.update(update_reception_params[:param])        
          if ((old_reception_status != @reception.status) && @reception.status == 2)
            UserMailer.appointment_confirmed(@reception).deliver_later
          end
          render json: {reception:@reception,message:message[:update_succes]}, status: 200
        else
          render json: { errors: message[:server_error] }, status: 500
        end
      else
        render json: { errors: update_reception_params[:message]}, status: 500
      end
    else
      render json: { errors: message[:no_found]}, status: 500
    end
  end
  ## Reject reception
  def delete
    @reception = Reception.find_by_id(params[:id])
    if @reception
        if @reception.destroy
          render json: {reception:@reception,message:message[:delete_succes]}, status: 200
        else
          render json: { errors: message[:server_error] }, status: 500
        end
    else
      render json: { errors: message[:no_found] }, status: 500
    end
  end
  def delete_reception_file
    @file = UserFile.find_by_id(params[:id])
    if @file
        if @file.destroy
          key = @file.url
          if  key != ''
          s3 = AWS::S3.new
          bucket = 'patientusers'
                b = s3.buckets[bucket]
                #delete S3 object 
                obj = b.objects[key]
                obj.delete
        end
          render json: {file:@file,message:"successful"}, status: 200
        else
          render json: { errors: message[:server_error] }, status: 410
        end
    else
      render json: { errors: "file no found"}, status: 410
    end
  end
  ##Display all
  def receptions
    @receptions = Reception.all
    if @receptions != []
      render json: {reception:@receptions,message:message[:found_success_s]}, status: 200
    else
      render json: { errors: message[:no_found_s] }, status: 500
    end
  end

  def get_users_with_appruve_receptions
    @receptions = Reception.where(:staff_id => params[:staff_id]).where(:status => 2)
    if @receptions != []
      @users_id = Array.new
      @users= Array.new
      for reception in @receptions
        if !(@users_id.include? reception.user_id)
          @user = User.find_by_id(reception.user_id)

          @users_id << reception.user_id
          @users << @user
        end
      end
      render json: {users_id: @users_id, users: @users}, status: 200
    else
      render json: { errors: @receptions }, status: 200
    end
  end

  ##Display the one
  def reception
    @reception = Reception.find_by_id(params[:id])
    if @reception
      render json: {reception:@reception, message:message[:found_success]}, status: 200
    else
      render json: { errors: message[:no_found_s]}, status: 500
    end
  end

  def reception_by_status
    @reception = Reception.where(:staff_id => params[:staff_id]).where(:status => params[:status])
    if @reception
      render json: {count:@reception.count, message:message[:found_success]}, status: 200
    else
      render json: { errors: message[:no_found_s]}, status: 410
    end
  end

  def reception_files  
    @files = UserFile.where(:reception_id => params[:id])
    if @files
      render json: {files:@files, message:'Files have found successfully'}, status: 200
    else
      render json: { errors: message[:no_found_s]}, status: 410
    end
  end
  

  private
  def update_reception_params
    @reception_params = params[:reception]
    if @reception_params !={}
        if !(@reception_params[:staff_id] || @reception_params[:user_id]|| @reception_params[:start_date]|| @reception_params[:end_date]|| @reception_params[:doctor_reason]|| @reception_params[:client_reason] || @reception_params[:status])
            return {is_success: false,message:message[:empty_fields]}
        else
          @errors = {}
          ## validate start_date
          if @reception_params[:start_date]
            if !DateTime.parsable?(@reception_params[:start_date])
            @errors.store("start_date",message[:incorrect_start_date])
            end
          end
          ## validate end_date
          if @reception_params[:end_date]
            if !DateTime.parsable?(@reception_params[:end_date])
            @errors.store("end_date",message[:incorrect_end_date])
            end
          end
          ## validate doctor_reason
          if @reception_params[:doctor_reason]==''
            @errors.store("doctor_reason",message[:empty_doctor_reason])
          end
          ## validate client_reason
          if @reception_params[:client_reason]==''
            @errors.store("client_reason", message[:empty_client_reason])
          end
          if @reception_params[:status]==''
            @errors.store("status",message[:empty_status])
          end

          if @errors.count > 0
              return {is_success: false,message:@errors}
          else
              @param = params.require(:reception).permit(:staff_id, :user_id, :start_date, :end_date, :doctor_reason, :client_reason, :status)
              return {is_success: true,param:@param}
          end
        end
    else
    return {is_success: false,message:message[:empty_fields]}
    end
    
  end

  private
  def reception_params
       @reception_params = params[:reception]
    if @reception_params !={}
        if !(@reception_params[:staff_id] || @reception_params[:user_id]|| @reception_params[:start_date]|| @reception_params[:end_date]|| @reception_params[:doctor_reason]|| @reception_params[:client_reason] || @reception_params[:status])
            return {is_success: false,message:message[:empty_fields]}
        else
          @errors = {}
          ## validate doctor_reason
          if @reception_params[:staff_id]=='' || !@reception_params[:staff_id]
            @errors.store("staff_id",message[:empty_staff_id])
          end
          ## validate client_reason
          if @reception_params[:user_id]=='' || !@reception_params[:user_id]
            @errors.store("user_id",message[:empty_user_id])
          end
          ## validate start_date
          if @reception_params[:start_date]
            if @reception_params[:start_date]
              if !DateTime.parsable?(@reception_params[:start_date])
              @errors.store("start_date",message[:incorrect_start_date])
              end
            end
          else
          @errors.store("start_date",message[:empty_start_date])
          end
          ## validate end_date
          if @reception_params[:end_date]
            if @reception_params[:end_date]
              if !DateTime.parsable?(@reception_params[:end_date])
              @errors.store("end_date",message[:incorrect_end_date])
              end
            end
          else
          @errors.store("end_date",message[:empty_end_date])
          end
         
          ## validate status
          if @reception_params[:status]=='' || !@reception_params[:status]
            @errors.store("status",message[:empty_status])
          end
          
          if @errors.count > 0
          return {is_success: false,message:@errors}
          else
          @param = params.require(:reception).permit(:staff_id, :user_id, :start_date, :end_date, :doctor_reason, :client_reason, :status)
          return {is_success: true,param:@param}
          end
        end
    else
    return {is_success: false,message:message[:empty_fields],param: @reception_params}
    end
  end
  

  private
    def reception_message
      params.require(:message).permit(:staff_id, :user_id, :sender_id, :body)
    end

end