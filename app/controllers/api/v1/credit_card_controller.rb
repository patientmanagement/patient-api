class Api::V1::CreditCardController < ApplicationController
  # before_filter :authenticate_request!

  # * +'visa'+
  # * +'master'+
  # * +'discover'+
  # * +'american_express'+
  # * +'diners_club'+
  # * +'jcb'+
  # * +'switch'+
  # * +'solo'+
  # * +'dankort'+
  # * +'maestro'+
  # * +'forbrugsforeningen'+
  # * +'laser'+


 def add_card

 	credit_card = ActiveMerchant::Billing::CreditCard.new(
	                :first_name         => 'Holdname',
	                :last_name          => 'Holdname',
	                :number             => card_params[:number],
	                :brand              => card_params[:card_type],
	                :month              => card_params[:month],
	                :year               => card_params[:year],
	                :verification_value => card_params[:cvc]).validate


 	@user_card_rel = UserCreditCard.where(user_id: params[:user_id])
  @credit_card_number = Array.new
  for relation in @user_card_rel
    @card = CreditCard.where(id: relation.credit_card_id).take
    if @card.number == card_params[:number]
      @credit_card_number = @card
    else
      @credit_card_number = []
    end
  end 
 	if credit_card == {}
      if @credit_card_number == []
    	 		@credit_card=CreditCard.new(card_params)
    	 		if @credit_card.save
    	 		   @user_card_relation=UserCreditCard.new({user_id: params[:user_id] , credit_card_id: @credit_card.id})
    	 		   @user_card_relation.save
    	 	  end		
	    	render json: { message: "Credit Card is successfully added." } , status:200
	    else
	    	render json: { errors: { exist:"This card number already registered."} } , status:200
      end
  else
  	render json: {errors:credit_card} , status:200
  end
 end

 def show_all_card
    @user_card_rel = UserCreditCard.where(user_id: params[:user_id])
    @cards = Array.new
    for relation in @user_card_rel
      @card = CreditCard.find(relation.credit_card_id)
      @card_elem = {
      	id: @card.id,
      	number: 'XXXXXXXXXXXX' + @card.number.last(4),
        full_number: @card.number,
      	month: @card.month,
      	year: @card.year,
      	card_type: @card.card_type,  
        cvc: @card.cvc
      }
      @cards << @card_elem
    end  
    render json: @cards
 end

 # Deleting users
 def destroy_card
    credit_card = CreditCard.find_by_id(params[:card_id])
    user_card_rel = UserCreditCard.where(user_id: params[:user_id], credit_card_id: params[:card_id]).take
    if credit_card && user_card_rel
      user_card_rel.destroy
	    credit_card.destroy
	    render json: { message: "Card successfully deleted."} , status:200
	else
		render json: { message: "Card not found."} , status:422
	end
 end

  def card_params
    params.require(:card).permit(:number, :month, :year, :cvc, :card_type)
  end

  def get_active_method
    if params[:user_id] == "superadmin"
      @superadmin = User.where(user_type: "superadmin").take
      params[:user_id] = @superadmin.id
    end
    if params[:subdomain]
      @company = Company.where(subdomain: params[:subdomain]).take
      if @company
        params[:user_id] = @company.user_id
      end
    end
    @stripe_key = SripeCredential.where(user_id: params[:user_id]).take
    if @stripe_key && @stripe_key.is_active
      render json: { active_method: "stripe" }, status: 200
    else
      @twocheckout_credentials = TwocheckoutCredential.where(user_id: params[:user_id]).take
      if @twocheckout_credentials && @twocheckout_credentials.is_active
        render json: { active_method: "twocheckout" }, status: 200
      else
        render json: { active_method: "" }, status: 200
      end
    end
  end

  def set_active_method
    @stripe_key = SripeCredential.where(user_id: params[:user_id]).take
    @twocheckout_credentials = TwocheckoutCredential.where(user_id: params[:user_id]).take
    if params[:active_method] == 'stripe'
      if @stripe_key
        @stripe_key.is_active = true
        @stripe_key.save
        if @twocheckout_credentials
          @twocheckout_credentials.is_active = false
          @twocheckout_credentials.save
        end
        render json: { success: "Stripe credentials were set as active" }, status: 200
      else
        render json: { errors: "You don't have stripe credentials" }, status: 500
      end
    else
      if params[:active_method] == 'twocheckout'
        if @twocheckout_credentials
          @twocheckout_credentials.is_active = true
          @twocheckout_credentials.save
          if @stripe_key
            @stripe_key.is_active = false
            @stripe_key.save
          end
          render json: { success: "2checkout credentials were set as active" }, status: 200
        else
          render json: { errors: "You don't have 2checkout credentials" }, status: 500
        end
      else
        render json: { errors: "Invalid method" }, status: 500
      end
    end
  end
end
