class Api::V1::RegisterFieldsController < ApplicationController
	def create
		if params[:company_id]
			@field = RegisterField.new({company_id: params[:company_id], title: params[:title]})
			if @field.save
				render json: {field: @field}, status: 200
			else
				render json: {errors: @field.errors}, status: 500
			end	
		else
			if params[:subdomain]
				@company = Company.where(subdomain: params[:subdomain]).take
				if @company
					@field = RegisterField.new({company_id: @company.id, title: params[:title]})
					if @field.save
						render json: {field: @field}, status: 200
					else
						render json: {errors: @field.errors}, status: 500
					end	
				else
					render json: {errors: "No such company"}, status: 500
				end
			else
				render json: {errors: "Invalid parameters"}, status: 500
			end
		end
	end

	def update
		@field = RegisterField.find_by_id(params[:id])
		if @field
			if @field.update({title: params[:title]})
				render json: {field: @field}, status: 200
			else
				render json: {errors: @field.errors}, status: 500
			end
		else
			render json: {errors: "Field not found"}, status: 500
		end
	end

	def destroy
		@field = RegisterField.find_by_id(params[:id])
		if @field
			if @field.destroy
				render json: {success: "Field was succesfully deleted"}, status: 200
			else
				render json: {errors: @field.errors}, status: 500
			end
		else
			render json: {errors: "Field not found"}, status: 500
		end
	end

	def show_all
		if params[:company_id]
			render json: RegisterField.where(company_id: params[:company_id])
		else
			if params[:subdomain]
				@company = Company.where(subdomain: params[:subdomain]).take
				if @company
					render json: RegisterField.where(company_id: @company.id)
				else
					render json: {errors: "No such company"}, status: 500
				end
			else
				render json: {errors: "Invalid parameters"}, status: 500
			end
		end
	end
end
