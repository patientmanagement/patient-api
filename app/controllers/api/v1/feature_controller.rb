class Api::V1::FeatureController < ApplicationController

	def create
		plan = Plan.find_by_id(params[:id])
		if plan
			feature = Feature.new({ plan: plan, title: feature_params[:title]})

			if feature.save
				render json: { feature: feature }, status: 200
			else 
				render json: { errors: feature.errors}, status: 500
			end
		else
			render json: { errors: "Plan not found" }, status: 404
		end
	end

	def update
		plan = Plan.find_by_id(params[:id])
		if plan
			feature = Feature.find(params[:feature_id])

			if feature.update(feature_params)

				render json: { feature: feature }, status: 200
			else 
				render json: { errors: feature.errors}, status: 500
			end
		else
			render json: { errors: "Plan not found" }, status: 404
		end
	end

	def delete
		begin
			plan = Plan.find_by_id(params[:id])
			if plan
				feature = Feature.find(params[:feature_id])

				if feature.destroy
					render json: { message: "Successfully deleted" }, status: 200
				else 
					render json: { errors: feature.errors}, status: 500
				end
			else
				render json: { errors: "Plan not found" }, status: 404
			end
		rescue ActiveRecord::InvalidForeignKey
			render json: { errors: "Feature registered to plan" }, status: 500
		end
	end

	def feature
		plan = Plan.find_by_id(params[:id])
		if plan
			feature = Feature.find(params[:feature_id])

			render json: { feature: feature }
		else
			render json: { errors: "Plan not found" }, status: 404
		end
	end

	def features
		# user_plans = UserPlan.where(user_id: params[:id])

		# plans = Array.new
		# for relation in user_plans
		# 	field = Plan.find(relation.plan_id)
		# 	plans << field
		# end
		plans = Feature.all

	    render json: plans
	end

	def feature_params
		params.require(:feature).permit(:title)
	end
end
