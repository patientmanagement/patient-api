class Api::V1::FieldController < ApplicationController
	def create
		user = User.find_by_id(params[:id])
		if user
			@field = Field.new(field_params)

			if @field.save
				user_field = UserField.new({
						user_id: params[:id],
						field_id: @field.id
					})
				if user_field.save
					render json: { field: @field }, status: 200
				else
					render json: { errors: @user_field.errors}, status: 500
				end
			else 
				render json: { errors: @field.errors}, status: 500
			end
		else
			render json: { errors: "User not found" }, status: 404
		end
	end

	def update
		user = User.find_by_id(params[:id])

		if user
			field = Field.find_by_id(params[:field_id])
	      	if field
		        if field.update(field_params)
		            render json: { field: field }, status: 200
		        else
		          	render json: { errors: field.errors }, status: 200
		        end
	      	else
	       		render json: { errors: "Field not found" }, status: 200
	      	end
	    else
	    	render json: { errors: "User not found" }, status: 404
	    end
	end

	def delete
		user = User.find_by_id(params[:id])
		if user
		    field = Field.find_by_id(params[:field_id])
		    if field

		    	rel = UserField.where(user_id: params[:id], field_id: params[:field_id]).take;
				rel.destroy if rel

				reldoc = FieldDocument.where(field_id: params[:field_id]);

				for relation in reldoc
			        doc = Document.find(relation.document_id)
			        relation.destroy
			        doc.destroy if doc
			    end 

			    if field.destroy

			        render json: { success: "Field successfully deleted" }
			    else
			        render json: { errors: "Field not deleted" }, status: 500
			    end
		    else
		      render json: { errors: "Field not found" }, status: 404
		    end
		else
			render json: { errors: "User not found" }, status: 404
		end
	end

	def field
		user = User.find_by_id(params[:id])
		if user

			field = Field.find_by_id(params[:field_id])

			if field
				render json: { field: field }
			else
				render json: { message: "Field not found" }, status: 404
			end
		else
			render json: { errors: "User not found" }, status: 404
		end
	end

	def fields
		# render json: params
		# return
		# user_fields = UserField.where(user_id: params[:id])

		# fields = Array.new
		# for relation in user_fields
		# 	field = Field.find(relation.field_id)
		# 	fields << field
		# end 
		fields = Field.all

	    render json: fields
	end

	def field_params
		params.require(:field).permit(:title)
	end
end
