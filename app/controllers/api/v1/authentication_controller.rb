class Api::V1::AuthenticationController < ApplicationController

  def authenticate_user
    user = User.find_for_database_authentication(email: user_params[:email])
    if user 
      if user.is_login
        return render json: { errors: 'User is already logged in' }
      end
      if user.is_active == "1"
        if user.is_verified == "1"
          if user.valid_password?(user_params[:password])
            if user.user_type == 'admin'
              company = Company.where(user_id: user.id).take
              unless company.nil? || company.status != 'Approve'
                render json: payload(user)
              else
                render json: {errors: 'Your company isn\'t approved yet.'}, status: 200
              end
            else
              render json: payload(user)
            end
          else
            render json: {errors: 'Invalid Username/Password.'}, status: 200
          end
        else
          render json: {errors: 'User is not verified.'}, status: 200
        end
      else
        render json: {errors: 'User is not active.'}, status: 200
      end
    else
      render json: {errors: 'User not found.'}, status: 200
    end
  end

  def logout
    # @current_user.update(:is_login => false) if @current_user
    user = User.find_by_id(params[:user_id])
    if user
      user.update(:is_login => false)
    end
    render json: { message: 'Successfully logout' }
  end

  private

  def payload(user)
    token = JsonWebToken.encode(user_id: user.id)
    user.update(
      :is_login => true, 
      :sign_in_count => user.sign_in_count.to_i + 1, 
      :current_sign_in_at => Time.now, 
      :current_sign_in_ip => request.remote_ip,
      :last_sign_in_at => user.last_sign_in_at,
      :last_sign_in_ip => user.last_sign_in_ip,
      :token => token
    ) if user

    return nil unless user and user.id
    {
      token: token,
      user: {id: user.id, email: user.email, type: user.user_type}
    }
  end

  private

  def user_params
   params.require(:user).permit(:email, :password)
  end


end
