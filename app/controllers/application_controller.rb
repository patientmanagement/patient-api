class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  before_filter :cors_preflight_check
  after_filter :cors_set_access_control_headers
  before_action :authenticate_request!, :except => [
    :authenticate_user, 
    :plans, 
    :fields, 
    :get_active_method, 
    :get_admin_paypal_credentials, 
    :create, 
    :verified_user,
    :companies,
    :show_all,
    :save_company_logo,
    :reset_pass,
    :reset_pass_link,
    :get_user_company,
    :show,
    :test,
    :logout,
    :get_payment_cred
  ]

  def cors_set_access_control_headers
    puts 'cors_set_access_control_headers'
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
    headers['Access-Control-Request-Method'] = '*'
    headers['Access-Control-Allow-Headers'] = 'Origin, Authorization,Content-Type, Accept, Authorization, Token'
    headers['Access-Control-Max-Age'] = "1728000"
  end

  def cors_preflight_check
    if request.method == 'OPTIONS'
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
      headers['Access-Control-Request-Method'] = '*'
      headers['Access-Control-Allow-Headers'] = 'X-Requested-With, Authorization, Content-Type, X-Prototype-Version, Token'
      headers['Access-Control-Max-Age'] = '1728000'

      render :text => '', :content_type => 'text/plain'
    end
  end

  attr_accessor :current_user



  protected
  def authenticate_request!
    unless user_id_in_token?
      render json: { errors: ['Not Authenticated'] }, status: :unauthorized
      return
    end
    @current_user = User.find(auth_token[:user_id])
  rescue JWT::VerificationError, JWT::DecodeError
    render json: { errors: ['Not Authenticated'] }, status: :unauthorized
  end

  private
  def http_token
      @http_token ||= if request.headers['Authorization'].present?
        request.headers['Authorization'].split(' ').last
      end
  end

  def auth_token
    @auth_token ||= JsonWebToken.decode(http_token)
  end

  def user_id_in_token?
    http_token && auth_token && auth_token[:user_id].to_i
  end

  def check_taken_space(company_id, new_size)
    @s3 = AWS::S3.new
    @bucket = @s3.buckets['patientusers']
    @size = 0
    @company = Company.find_by_id(company_id)
    @bucket.objects.each do |object|
      if @company.logo.include? object.key
        @size += object.content_length
      end
      @company.user.each do |user|
        if user.logo.include? object.key
          @size += object.content_length
        end
        UserDocument.all.each do |document|
          if document.user_id == user.id
            if document.file_contents.include? object.key
              @size += object.content_length
            end
          end
        end
      end
    end
    @last_payment = Payment.where(user_id: @company.user_id).last
    if @last_payment.nil?
      return render json: { errors: "Company didn't choose plan yet" }
    end
    plan = Plan.find(@last_payment.plan_id)
    if plan.memory * 1024 * 1024 * 1024 > @size + new_size
      return true
    else
      return false
    end
  end
end
