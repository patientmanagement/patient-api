class StripeMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.stripe_mailer.stripe_mailer.subject
  #
  def stripe_mailer(user, reception_id)
  	@company_address = @host[:protocol] + user[:company].subdomain + '.' + @host[:hostname]
    @reception = Reception.find_by_id(reception_id)
    @user = user
    mail to: @user[:email] , subject: "Stripe Email " + @company_address 
  end
end
