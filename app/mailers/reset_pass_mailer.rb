class ResetPassMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.reset_pass_mailer.reset_pass_confirmation.subject
  #
  def reset_pass_confirmation(user)
    @user = user

    exp = Time.now.to_i + 60 * 5

		exp_payload = { :exp => exp, id: user.id, email: user.email  }

    @company_address = @host[:protocol] + user.company.subdomain + '.' + @host[:hostname]

    @token = JWT.encode exp_payload, Rails.application.secrets.secret_key_base
    mail to: @user.email , subject: "Password Reset link for " + @company_address
  end
end
