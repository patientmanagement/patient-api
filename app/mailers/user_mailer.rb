class UserMailer < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.registration_confirmation.subject
  #

  def registration_confirmation(user, password)
    @user = user

    @staff_password = password
    @company_address = @host[:protocol] + user.company.subdomain + '.' + @host[:hostname]

    mail to: @user.email , subject: "You Are Now Registered with " + @company_address
  end

  def simple_mail
    mail to: "horoshko.m@gmail.com" , subject: "simple mail runner"
  end

  def notification_charge(user, amount, not_count)
    @user = user
    @amount = amount / 100
    @not_count = not_count
    @company_address = @host[:protocol] + user.company.subdomain + '.' + @host[:hostname]
    mail to: user.email , subject: "Notification"
  end

  def successfully_charge(user, amount)
    @user = user
    @company_address = @host[:protocol] + user.company.subdomain + '.' + @host[:hostname]
    mail to: user.email , subject: "Your Payment has been Successful for " + @company_address
  end

  def unsuccessfully_charge(user, amount, e)
    @amount = amount / 100
    @company_address = @host[:protocol] + user.company.subdomain + '.' + @host[:hostname]
    mail to: user.email , subject: "Unsuccessfully Charge for " + @company_address
  end

  def appoinment_notification(mail_to_user, appointment_user, minutes)
    app = mail_to_user.company.company_menu_item.find_by(menu_item_title: 'Appointment')
    @appointment = app ? app.title : 'Appointment'
    @mail_to_user = mail_to_user
    @user = appointment_user
    @minutes = minutes
    @company_address = @host[:protocol] + mail_to_user.company.subdomain + '.' + @host[:hostname]
    
    mail to: mail_to_user.email , subject: "Notification for " + @company_address
  end

  def appointment_confirmed(appointment)
    @appointment = appointment
    @user = appointment.user
    @company_address = @host[:protocol] + @user.company.subdomain + '.' + @host[:hostname]
    mail to: @user.email, subject: "Your Appointment is Confirmed by " + @company_address
  end

  def end_of_membership(user)
    @user = user
    @company_address = @host[:protocol] + user.company.subdomain + '.' + @host[:hostname]
    
    mail to: @user.email, subject: "Your Membership is about to end with " + @host[:hostname]
  end
end
