class CompanyMail < ApplicationMailer
	def send_company(user, status)
		@user = user
		@company_address = @host[:protocol] + @company.subdomain + '.' + @host[:hostname]
	  @status = status

	  mail to: @user.email , subject: "Company Status Changed " + @company_address
	end

	def company_approved(user)
		@company = user.company
		@client = user
		@company_address = @host[:protocol] + @company.subdomain + '.' + @host[:hostname]
		mail to: @client.email , subject: "Congratulations, your Company is now registered with " + @host[:hostname]
	end

	def company_rejected(user)
		@company = user.company
		@client = user
		@company_address = @host[:protocol] + @company.subdomain + '.' + @host[:hostname]
		mail to: @client.email , subject: "Your account is under review with the Management at " + @host[:hostname]
	end
end
