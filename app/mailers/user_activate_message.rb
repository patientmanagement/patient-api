class UserActivateMessage < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_activate_message.user_activate_message.subject
  #
  def user_activate_message(user)
    @user = user

    mail to: @user.email , subject: "User Activate"
  end
end
