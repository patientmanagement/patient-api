class PrescriptionMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.prescription_mailer.PrescriptionMailer.subject
  #
  def PrescriptionMailer(prescription)
  	@prescription = prescription
    if  @prescription[:user_type]=='user'
       mail to: @prescription[:user_email] , subject: "Prescription added to reception #"+@prescription[:reception_id].to_s
    else
      	if @prescription[:file_name] && @prescription[:file_path]
        	attachments[@prescription[:file_name]] = File.read(@prescription[:file_path])
        mail from:@prescription[:sender_email] , to: @prescription[:pharmacy_email] , subject: "Prescription" ,add_file: @prescription[:file_path]
        else
        mail from:@prescription[:sender_email] , to: @prescription[:pharmacy_email] , subject: "Prescription"
        end
    end
  end
end
