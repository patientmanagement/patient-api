class ApplicationMailer < ActionMailer::Base
  default from: "from@example.com"
  layout 'mailer'
  before_action :host

  def host
    if !@host
      @host = Rails.configuration.configData.content[:host]
    end
    return @host
  end
end
