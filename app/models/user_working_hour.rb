class UserWorkingHour < ActiveRecord::Base
	belongs_to :user
	belongs_to :working_hour, foreign_key: "working_hours_id"
end
