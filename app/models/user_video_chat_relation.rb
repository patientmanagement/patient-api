class UserVideoChatRelation < ActiveRecord::Base
	validates :user_id, presence: { message: "user_id is required." }
	validates :video_chat_id, presence: { message: "video_chat_id is required." }
end
