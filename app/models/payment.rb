class Payment < ActiveRecord::Base
  belongs_to :user
  belongs_to :credit_card
  belongs_to :plan

  def company
  	Company.where(user_id: self.user.id).take
  end
end
