class SripeCredential < ActiveRecord::Base
	validates :key, presence: { message: "Stripe key is required." }
end
