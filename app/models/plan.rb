class Plan < ActiveRecord::Base
	has_many :feature
	
	has_many :user_plan
	has_many :user, through: :user_plan

	def features
		Feature.where(plan_id: self.id)
	end
end
