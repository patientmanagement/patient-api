require 'time'
class User < ActiveRecord::Base

	has_one :company_user
	has_one :company, through: :company_user

	has_one :profession_user, dependent: :destroy
	has_one :profession, through: :profession_user, dependent: :destroy

    has_one :user_plan
    has_one :plan, through: :user_plan

	has_many :reception
	has_many :staff_receptions, class_name: "Reception", foreign_key: "staff_id"

	has_one :speciality

	has_many :user_working_hour
	has_many :working_hour, through: :user_working_hour
    # belongs_to :company, through: :company_user
    
	validates :logo, length: { minimum: 6 }, allow_blank: true
	validates :is_verified, inclusion: { in: ["0","1"], message: "must be given '0' or '1'"}, allow_blank: true
	validates :is_active, inclusion: { in: ["0","1"], message: "must be given '0' or '1'" }, allow_blank: true
	validates :first_name, presence: { message: "must be given please" }, length: { minimum: 2 , message: "first_name is too short (minimum is 2 characters)" }
	validates :last_name, presence: true, length: { minimum: 2 , message: "last_name is too short (minimum is 2 characters)"}
	validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i , message: "please , enter valid email"}
	validates :user_type, presence: { message: "must be given please" }, inclusion: { in: ['user', 'admin', 'superadmin', 'staffmember'], message: "field types: superadmin, admin, user, stuffmember."}
	validates :password, format: { with: /(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z])/,
    message: "Must contain one lower &amp; uppercase letter, and one non-alpha character (a number or a symbol.), must be between 6 and 20 characters." }, allow_blank: true
    # validates_with UserTypeValidator, fields: [:user_type]

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
    
    def as_json(options = { })
	    h = super(options)
	    h[:profession]   = self.profession
	    h[:speciality]   = self.speciality
	    h
	end

    def self.admin_payment_notify

    	self.where(user_type: 'admin').find_each do |user|
            next if !user.company
    		payment = Payment.where(user_id: user.id).order(:created_at).last

    		if payment 

	    		plan = Plan.find(payment.plan_id)

				count_days = 0
				not_count = 0
				case plan.plan_type
				when 'weekly'
					count_days = 6
					not_count = 1
				when 'monthly'
					count_days = 23
					not_count = 7
				when 'yearly'
					count_days = 358
					not_count = 7
				end
                puts (Date.today - (Time.parse(payment.created_at.to_s).to_date + count_days)).to_i
    			if ((Date.today - (Time.parse(payment.created_at.to_s).to_date + count_days)).to_i == 0)
    				# UserMailer.simple_mail.deliver_now
    				UserMailer.notification_charge(user, payment.amount, not_count).deliver_now
				end 	
    		end
    	end

    end

    def self.admin_payment_charge
    	self.where(user_type: 'admin').find_each do |user|
            next if !user.company
    		payment = Payment.where(user_id: user.id).order(:created_at).last
    		if payment 
    			plan = Plan.find(payment.plan_id)

    			count_days = 0
    			case plan.plan_type
    			when 'weekly'
    				count_days = 7
    			when 'monthly'
    				count_days = 30
    			when 'yearly'
    				count_days = 365
    			end
    			if ((Date.today - (Time.parse(payment.created_at.to_s).to_date + count_days)).to_i == 0)
    				# UserMailer.simple_mail.deliver_now
    				card = CreditCard.find(payment.credit_card_id)

    				# UserMailer.notification_charge(user, payment.amount).deliver_now
                    begin
                        stripeToken = Stripe::Token.create(
                            :card => {
                              :number => card.number,
                              :exp_month => card.month,
                              :exp_year => card.year,
                              :cvc => card.cvc
                            },
                        )

                        customer = Stripe::Customer.create(
                            :email => user.email,
                            # :source  => payment_params[:stripeToken]
                            :source  => stripeToken.id

                        )

                        charge = Stripe::Charge.create(
                            :customer    => customer.id,
                            :amount      => payment.amount.to_i,
                            :description => 'Rails Stripe customer',
                            :currency    => 'usd'
                        )
                        puts "Payment successfully charged", user.id, payment.amount
                        UserMailer.successfully_charge(user, payment.amount).deliver_later
                    rescue Stripe::CardError => e
                        puts "Stripe payment error: ", e
                        UserMailer.unsuccessfully_charge(user, payment.amount, e).deliver_later
                        UserMailer.end_of_membership(user).deliver_later
                    rescue Exception => e
                        UserMailer.unsuccessfully_charge(user, payment.amount, e).deliver_later
                        UserMailer.end_of_membership(user).deliver_later
                        puts "Some error corrupted: ", e
                    end
                

					
					# rescue Stripe::CardError => e
					# 	UserMailer.unsuccessfully_charge(user, payment.amount, e).deliver_now
				end 	
    		end
    	end
    end

    def self.notify_user_reception
    	default_minutes = 15

    	recps = Reception.where("(receptions.start_date BETWEEN now() + (#{default_minutes} * interval '1 minute') AND now() + (#{default_minutes + 1} * interval '1 minute')) AND receptions.status = 2")
    	recps.find_each do |reception|
    		user = reception.user
    		staff = reception.staff
    		UserMailer.appoinment_notification(staff, user, default_minutes).deliver_later
    		UserMailer.appoinment_notification(user, staff, default_minutes).deliver_later
            puts "Notification email sends to users: #{user.first_name + ' ' + user.last_name}, #{staff.first_name + ' ' + staff.last_name}, time: #{Time.now}"
    		logger.debug "Notification email sends to users: #{user.first_name + ' ' + user.last_name}, #{staff.first_name + ' ' + staff.last_name}, time: #{Time.now}"
    	end
    end

    def self.check_reception_status

        receptions = Reception.where("receptions.start_date BETWEEN now() - interval '1 minute' AND now()")
        receptions.find_each do |reception|
            @payment_status = PaymentHistory.where(reception_id:reception.id).take
            if (reception.status != 2 || (reception.status == 2 && !@payment_status)) && (reception.status != 5)
                reception.update(:status => 5)
            end
        end
        
    end
    def self.check_token_exp
        self.where(:is_login => true).find_each do |user|
            if user.token
                decoded_token = JsonWebToken.decode(user.token)
                if decoded_token
                    if Time.now.to_i > decoded_token[:exp]
                        logger.debug "user updated: #{user.update(:is_login => false)}"
                    end
                else
                    user.update(:is_login => false)
                end
            else
                user.update(:is_login => false)
            end

        end
    end
end
