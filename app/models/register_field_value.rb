class RegisterFieldValue < ActiveRecord::Base
	belongs_to :register_field
	def register_field_data
		RegisterField.find(self.register_field.id)
	end
end
