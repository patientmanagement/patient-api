class VideoChatMessage < ActiveRecord::Base
	belongs_to :video_chat
	belongs_to :user

	def as_json(options = { })
	    h = super(options)
	    h[:sender]   = self.user
	    h
	end
end
