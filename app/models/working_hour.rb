class WorkingHour < ActiveRecord::Base
	validates :day, inclusion: { in: ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'], message: "please enter valid day."}, presence: { message: "day is required." }
	validates :start_time, presence: { message: "start_time is required." }
	validates :end_time, presence: { message: "end_time is required." }
	# validates_with DateTimeValidator, fields: [:start_time, :end_time]
end
