class Profession < ActiveRecord::Base
	belongs_to :company
	has_many :profession_user
	has_many :user, through: :profession_user, dependent: :delete_all

	def count_staff
		self.user.count
	end
end
