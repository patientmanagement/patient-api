class UserPrice < ActiveRecord::Base
	validates :user_id, presence:true ,allow_blank: true
	validates :amount, presence:true ,numericality: { only_integer: false }
end
