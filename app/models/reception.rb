class Reception < ActiveRecord::Base
	has_one :payment_history
	belongs_to :user
	belongs_to :staff, class_name: "User"
end
