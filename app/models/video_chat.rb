class VideoChat < ActiveRecord::Base

	has_many :video_chat_message

	validates :is_active, inclusion: { in: [ 0 , 1], message: "please enter valid field is_active, must be 0 or 1"}
	validates :video_chat_id, presence: { message: "video_chat_id is required." }
	validates :video_chat_name, presence: { message: "video_chat_name is required." }
	validates :start_video_chat, presence: { message: "end_repeat is required, must be date format" }
	validates :end_video_chat, presence: { message: "end_repeat is required, must be date format" }, allow_blank: true

end
