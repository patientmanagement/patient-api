class Break < ActiveRecord::Base
	validates :repeat_type, inclusion: { in: ['everyday', 'everyweek', 'everymonth', 'everyyear', 'none'], message: "please enter valid repeat_type."}, presence: { message: "repeat_type is required." }
	validates :start_day, presence: { message: "start_day is required, must be date format" }
	validates :end_day, presence: { message: "end_day is required, must be date format" }
	validates :end_repeat, presence: { message: "end_repeat is required, must be date format" }

    validate :validate_end_date_before_start_date

    def validate_end_date_before_start_date
	  	if start_day && end_day && end_repeat
		    if start_day >= end_day  
		      errors.add(:end_day, "start_day must be less than end_day.")
		    end

		    if end_day > end_repeat  
		      errors.add(:end_repeat, "end_day must be less or equal end_repeat.")
		    end
	    end
    end
end
