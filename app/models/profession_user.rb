class ProfessionUser < ActiveRecord::Base
  belongs_to :profession
  belongs_to :user
end
