class CreditCard < ActiveRecord::Base
	
    validates :number, presence: true
    validates :month, presence:  true
    validates :year, presence:  true
    validates :cvc, presence:  true
    validates :card_type, presence:  true
    
end
