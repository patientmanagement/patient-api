class Company < ActiveRecord::Base
  # belongs_to :user
  has_many :company_user
  has_many :user, through: :company_user, dependent: :delete_all
  has_many :company_menu_item, dependent: :delete_all
  has_many :profession, dependent: :destroy
end
