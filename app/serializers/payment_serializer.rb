class PaymentSerializer < ActiveModel::Serializer
  attributes :id, :amount, :description, :company, :created_at, :updated_at
  has_one :user
  has_one :credit_card
  has_one :plan
end
