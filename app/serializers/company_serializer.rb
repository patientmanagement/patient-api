class CompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :address, :email, :color, :logo, :status, :subdomain, :approve_key, :created_at, :updated_at
  has_many :user
  has_many :company_menu_item
  has_many :profession
end
