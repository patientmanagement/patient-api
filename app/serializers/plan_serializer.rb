class PlanSerializer < ActiveModel::Serializer
  attributes :id, :title, :price, :description, :plan_type, :color, :text_color, :created_at, :updated_at
  has_many :feature
end
