class UserDocumentSerializer < ActiveModel::Serializer
  attributes :id, :filename, :content_type, :file_contents
  has_one :user
  has_one :document
end
