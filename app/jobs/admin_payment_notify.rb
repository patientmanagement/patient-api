class AdminPaymentNotify
 @queue = :admin_payment_notify

 def self.perform
    User.admin_payment_notify
 end
end
