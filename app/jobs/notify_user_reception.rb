class NotifyUserReception
 @queue = :notify_user_reception

 def self.perform
    User.notify_user_reception
 end
end
