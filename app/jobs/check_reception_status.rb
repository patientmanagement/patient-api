class CheckReceptionStatus
 @queue = :check_reception_status

 def self.perform
    User.check_reception_status
 end
end
