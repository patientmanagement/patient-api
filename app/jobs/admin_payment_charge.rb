class AdminPaymentCharge
 @queue = :admin_payment_charge

 def self.perform
    User.admin_payment_charge
 end
end
