class CheckTokenExpiration
 @queue = :check_token_expiration

 def self.perform
    User.check_token_exp
 end
end
