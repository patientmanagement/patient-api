require 'resque/server'
Rails.application.routes.draw do

  # API routes path
  mount Resque::Server.new, at: "/resque"
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      #devise_for :users
      # resources :users, :only => [:show, :create, :update, :destroy]
      #get all users
      match 'users/:id', to: 'users#update', via: [ :put, :options]
      match 'users', to: 'users#create', via: [ :post, :options]
      get 'users/:id', to: 'users#show'
      get 'staffmembers', to: 'users#show_all_staffmember'
      get 'check_taken_space', to: 'company#check_taken_space'
      match "users/:id/receptions", to: "users#get_receptions" , via: [ :get, :options]
      match 'users/:id', to: 'users#destroy', via: [ :delete, :options]
      match "verified_user/:id", to: "users#verified_user", via: [ :put, :options]
      match "activate_user/:id", to: "users#activate_user", via: [ :put, :options]

      match 'users/:id/statistics', to: 'users#get_statistics', via: [ :get, :options]

      get 'users', to: 'users#show_all'

      match 'users/upload_logo' , to: 'users#upload_logo_to_s3', via: [ :post, :options]
      match 'users/upload_file' , to: 'users#upload_file_to_s3', via: [ :post, :options]

      match 'login', to: 'authentication#authenticate_user', via: [ :post, :options]
      match 'logout', to: 'authentication#logout', via: [ :post, :options]
      
      match 'users/:id/change_pass' , to: 'users#change_password', via: [ :post, :options]
      match "reset_pass", to: "users#reset_pass_link", via: [ :post, :options]
      match "reset_pass", to: "users#reset_pass", via: [ :put, :options]

      match 'users/:id/staff' , to: 'users#create_staff', via: [ :post, :options]
      get "users/:id/staff", to: "users#show_all_staff"

      # match "users/:id/:type", to: "users#show_all_users", via: [ :get, :options]

      match "staffmembers/find", to: "users#find_staffmembers", via: [ :get, :options]
      match "staffmember/find", to: "users#find_staffmember", via: [ :get, :options]

      get "users/:id/staff/:staff_id", to: "users#show_staff"
      match "users/:id/staff/:staff_id", to: "users#update_staff", via: [ :put, :options]
      match "users/:id/staff/:staff_id", to: "users#destroy_staff", via: [ :delete, :options]

      match "credit_card/:user_id", to: "credit_card#add_card", via: [ :post, :options]
      match "credit_card/:user_id", to: "credit_card#show_all_card" , via: [ :get, :options]
      match "credit_card/:user_id/:card_id", to: "credit_card#destroy_card" , via: [ :delete, :options]
      match "credit_card/:user_id/active_method", to: "credit_card#get_active_method", via: [ :get, :options]
      match "credit_card/:user_id/active_method", to: "credit_card#set_active_method" , via: [ :post, :options]

      match "working_hours/:user_id", to: "users#create_working_hours", via: [ :post, :options]    
      match "working_hours/:id", to: "users#delete_working_hours", via: [ :delete, :options]  
      get "working_hours/:user_id", to: "users#get_woring_hours"
      match "working_hours/:user_id/:working_hours_id", to: "users#destroy_working_hours", via: [ :delete, :options]  
      match "working_hours/:user_id/:working_hours_id", to: "users#update_working_hours", via: [ :put, :options]

      match "break/:user_id", to: "users#create_breaks", via: [ :post, :options] 
      get "break/:user_id", to: "users#get_breaks"
      match "break/:user_id/:break_id", to: "users#destroy_break", via: [ :delete, :options]
      match "break/:user_id/:break_id", to: "users#update_break", via: [ :put, :options]


      ##Receptions
      match 'receptions', to: 'receptions#create', via: [ :post, :options]
      match 'receptions/:id', to: 'receptions#update', via: [ :put, :options]
      match 'receptions/:id', to: 'receptions#delete', via: [ :delete, :options]   
      get "receptions", to: "receptions#receptions"
      match "appruve_receptions/:staff_id", to: "receptions#get_users_with_appruve_receptions", via: [ :get, :options] 
      get "receptions/status", to: "receptions#reception_by_status"
      get "receptions/:id", to: "receptions#reception"

      match "receptions/:id/files", to: "receptions#reception_files" , via: [ :get, :options]
      match "receptionfile/:id", to: "receptions#delete_reception_file" , via: [ :delete, :options]

      get "message", to: "reception_message#receptions_messages"
      match "users/:id/friends", to: "users#user_friends", via: [ :get, :options]
      match "users/:id/chat", to: "users#user_chat", via: [ :get, :options]

      match 'message/:id', to: 'reception_message#delete', via: [ :delete, :options]
      match 'receptions/:id/message', to: 'reception_message#create', via: [ :post, :options]
      get 'receptions/:id/message', to: 'reception_message#reception_messages'

      ## Amazon S3 Uploading
      post 'upload_video' , to: 'upload_to_amazon#uploadVideoToAmazon'
      match 'save_file_to_s3', to: 'upload_to_amazon#save_file_to_s3', via: [ :post, :options]
      match 'remove_file_from_s3', to: 'upload_to_amazon#remove_file_from_s3', via: [ :delete, :options]
      match 'remove_folder_from_s3', to: 'upload_to_amazon#remove_folder_from_s3', via: [ :delete, :options]
      match 'delete_test', to: 'upload_to_amazon#delete_test', via: [ :delete, :options]
      match 'get_video' , to: 'upload_to_amazon#getVideos', via: [ :get, :options]

      ##Video Chat
      match 'video_chat/:user_id', to: 'video_chat#create', via: [ :post, :options]
      match 'video_chat/:user_id/:video_chat_id', to: 'video_chat#create_user_chat_rell', via: [ :post, :options]
      match 'video_chat/:id', to: 'video_chat#update', via: [ :put, :options]
      match 'video_chat/:user_id/:video_chat_id', to: 'video_chat#delete', via: [ :delete, :options]   
      get "video_chat/:user_id", to: "video_chat#all_video_chat"
      get "video_chat/is_active/:session_id", to: "video_chat#get_video_chat"

      match 'video_chat_messages', to: 'video_chat#add_message', via: [ :post, :options ]
      get "video_chat/:id/messages", to: "video_chat#get_video_chat_messages"

      get "user_payment", to: "users#get_payment_cred"


      match 'twiddla', to: 'video_chat#start_twiddla', via: [:post, :options]
      match 'whiteboard', to: 'video_chat#whiteboard', via: [:post, :options]
      #Fields
      match 'users/:id/fields', to: 'field#create', via: [ :post, :options]
      match 'users/:id/fields/:field_id', to: 'field#update', via: [ :put, :options]
      match 'users/:id/fields/:field_id', to: 'field#delete', via: [ :delete, :options]   
      match "fields", to: "field#fields", via: [ :get, :options]
      match "fields/:field_id", to: "field#field",  via: [ :get, :options]

      #Documents
      match 'fields/:id/documents', to: 'document#create', via: [ :post, :options]
      match 'fields/:id/documents/:doc_id', to: 'document#update', via: [ :put, :options]
      match 'fields/:id/documents/:doc_id', to: 'document#delete', via: [ :delete, :options]   
      get "fields/:id/documents", to: "document#docs"
      get "fields/:id/documents/:doc_id", to: "document#doc"

      #Plans
      match 'users/:id/plans', to: 'plan#create', via: [ :post, :options]
      match 'users/:id/plans/:plan_id', to: 'plan#update', via: [ :put, :options]
      match 'users/:id/plans/:plan_id', to: 'plan#delete', via: [ :delete, :options]   
      match "plans", to: "plan#plans", via: [ :get, :options]
      get "plans/:plan_id", to: "plan#plan"

      #Company
      match 'companies', to: 'company#create', via: [ :post, :options]
      match 'companies/:id', to: 'company#update', via: [ :put, :options]
      match 'companies/:id', to: 'company#delete', via: [ :delete, :options]   
      match "companies", to: "company#companies", via: [ :get, :options]
      get "companies/:id", to: "company#company"
      match "users/:id/company", to: "company#get_user_company", via: [ :get, :options]   
      match 'save_company_logo', to: 'company#save_company_logo', via: [ :post, :options]
      match "export/companies", to: "company#export", via: [ :get, :options]
      match "export/payments", to: "payments#export", via: [ :get, :options]

      #Charge
      match 'charge', to: 'charges#create', via: [ :post, :options]
      match 'charge', to: 'charges#show', via: [ :get, :options]
      match 'charge/export', to: 'charges#export', via: [ :get, :options]

      # User price
      match 'users/:user_id/price', to: 'users#add_user_price', via: [ :post, :options]
      match 'price/:id', to: 'users#update_user_price', via: [ :put, :options]
      match 'price/:id', to: 'users#delete_user_price', via: [ :delete, :options]   
      match "user_price/:user_id", to: "users#get_user_price", via: [ :get, :options]

      # Spripe
      match 'stripe', to: 'stripe#create', via: [ :post, :options]
      match 'paypal', to: 'paypal#create', via: [ :post, :options]
      match 'paypal/pay', to: 'paypal#pay', via: [ :post, :options]
      match 'paypal/save', to: 'paypal#save_payment', via: [ :get, :options]
      match 'paypal/save_admin', to: 'paypal#save_admin_payment', via: [ :get, :options]
      match 'twocheckout', to: 'twocheckout#create', via: [ :post, :options]
      match 'twocheckout/pay', to: 'twocheckout#pay', via: [ :post, :options]
      # Payment History
      match "payment_history", to: "stripe#all_payment_history", via: [ :get, :options]
      match "user_payment_history/:id", to: "stripe#payment_history", via: [ :get, :options]
      match "payment_history/:id", to: "stripe#payment_history_delete", via: [ :delete, :options]
      # Stripe credentials
      match 'users/:id/stripe_key', to: 'users#add_admin_stripe_key', via: [ :post, :options]
      match "stripe_key", to: "users#get_all_stripe_key", via: [ :get, :options]
      match "users/:user_id/stripe_key", to: "users#get_admin_stripe_key", via: [ :get, :options]
      match "users/:user_id/stripe_key/:id", to: "users#update_admin_stripe_key", via: [ :put, :options]
      match "users/:user_id/stripe_key/:id", to: "users#delete_admin_stripe_key", via: [ :delete, :options]
      
      # Paypal credentials
      match 'users/:user_id/paypal', to: 'users#add_admin_paypal_credentials', via: [ :post, :options]
      match 'users/:user_id/paypal', to: 'users#get_admin_paypal_credentials', via: [ :get, :options]
      match 'users/:user_id/paypal/:id', to: 'users#update_admin_paypal_credentials', via: [ :put, :options]
      match 'users/:user_id/paypal/:id', to: 'users#delete_admin_paypal_credentials', via: [ :delete, :options]

      # 2checkout credentials
      match 'users/:user_id/twocheckout', to: 'users#add_admin_twocheckout_credentials', via: [ :post, :options]
      match "users/:user_id/twocheckout", to: "users#get_admin_twocheckout_credentials", via: [ :get, :options]
      match "users/:user_id/twocheckout/:id", to: "users#update_admin_twocheckout_credentials", via: [ :put, :options]
      match "users/:user_id/twocheckout/:id", to: "users#delete_admin_twocheckout_credentials", via: [ :delete, :options]

      match 'prescription', to: 'prescriptions#create', via: [ :post, :options]
      match 'prescription', to: 'prescriptions#get', via: [ :get, :options]
      match 'prescription', to: 'prescriptions#update', via: [ :put, :options]


      
      # resources :users do 
      #       resources :user_documents
      # end
      #UserDocuments
      match 'users/:id/user_documents', to: 'user_documents#create', via: [ :post, :options]
      match 'users/:id/user_documents/:user_document_id', to: 'user_documents#update', via: [ :put, :options]
      match 'users/:id/user_documents/:user_document_id', to: 'user_documents#update', via: [ :patch, :options]
      match 'users/:id/user_documents/:user_document_id', to: 'user_documents#destroy', via: [ :delete, :options]   
      get "users/:id/user_documents", to: "user_documents#index"
      get "users/:id/user_documents/:user_document_id", to: "user_documents#show"

      #PlanFeatures
      match 'plan/:id/features', to: 'feature#create', via: [ :post, :options]
      match 'plan/:id/features/:feature_id', to: 'feature#update', via: [ :put, :options]
      match 'plan/:id/features/:feature_id', to: 'feature#update', via: [ :patch, :options]
      match 'plan/:id/features/:feature_id', to: 'feature#delete', via: [ :delete, :options] 
      get "plan/:id/features", to: "feature#index"
      get "plan/:id/features/:feature_id", to: "feature#show"  


      match "users/:id/:type", to: "users#show_all_users", via: [ :get, :options]


      #Menu Items
      match 'companies/:company_id/menu_items', to: 'menu_items#create', via: [ :post, :options]
      match "companies/:company_id/menu_items", to: "menu_items#get", via: [ :get, :options ]
      match "companies/:company_id/menu_items/:menu_item_id", to: "menu_items#update", via: [ :put, :options ]
      # get "company/:id/user_documents/:user_document_id", to: "user_documents#show"

      #Profession
      match 'companies/:company_id/professions', to: 'professions#create', via: [ :post, :options]
      match 'companies/:company_id/professions/:id', to: 'professions#update', via: [ :put, :options]
      match 'companies/:company_id/professions/:id', to: 'professions#update', via: [ :patch, :options]
      match 'companies/:company_id/professions/:id', to: 'professions#destroy', via: [ :delete, :options]   
      get "companies/:company_id/professions", to: "professions#index"
      get "companies/:company_id/professions/:id", to: "professions#show"
      match 'companies/:company_id/professions/:id/users', to: 'professions#get_users', via: [ :get, :options]
      match 'companies/:company_id/professions/:id/users', to: 'professions#add_user_to_profession', via: [ :post, :options ]
      match 'companies/:company_id/professions/:id/users', to: 'professions#remove_user_from_profession', via: [ :delete, :options ]

      #Register fields
      match 'register_fields', to: 'register_fields#create', via: [:post, :options]
      match 'register_fields', to: 'register_fields#show_all', via: [:get, :options]
      match 'register_fields/:id', to: 'register_fields#update', via: [:put, :options]
      match 'register_fields/:id', to: 'register_fields#destroy', via: [:delete, :options]

      match 'test', to: 'users#test', via: [:get, :options]
    end
  end  
end