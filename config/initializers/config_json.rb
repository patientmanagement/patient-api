config_json = Rails.application.config_for :config_json

Rails.application.configure do
  config.configData= ActiveSupport::OrderedOptions.new
  config.configData.content = config_json
end