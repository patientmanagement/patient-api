require 'test_helper'

class UserActivateMessageTest < ActionMailer::TestCase
  test "user_activate_message" do
    mail = UserActivateMessage.user_activate_message
    assert_equal "User activate message", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
