require 'test_helper'

class StripeMailerTest < ActionMailer::TestCase
  test "stripe_mailer" do
    mail = StripeMailer.stripe_mailer
    assert_equal "Stripe mailer", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
