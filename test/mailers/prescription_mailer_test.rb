require 'test_helper'

class PrescriptionMailerTest < ActionMailer::TestCase
  test "PrescriptionMailer" do
    mail = PrescriptionMailer.PrescriptionMailer
    assert_equal "Prescriptionmailer", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
