# Preview all emails at http://localhost:3000/rails/mailers/user_activate_message
class UserActivateMessagePreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/user_activate_message/user_activate_message
  def user_activate_message
    UserActivateMessage.user_activate_message
  end

end
