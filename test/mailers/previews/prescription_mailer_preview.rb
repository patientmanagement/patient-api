# Preview all emails at http://localhost:3000/rails/mailers/prescription_mailer
class PrescriptionMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/prescription_mailer/PrescriptionMailer
  def PrescriptionMailer
    PrescriptionMailer.PrescriptionMailer
  end

end
