# Preview all emails at http://localhost:3000/rails/mailers/reset_pass_mailer
class ResetPassMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/reset_pass_mailer/reset_pass_confirmation
  def reset_pass_confirmation
    ResetPassMailer.reset_pass_confirmation
  end

end
