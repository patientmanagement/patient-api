require 'test_helper'

class ResetPassMailerTest < ActionMailer::TestCase
  test "reset_pass_confirmation" do
    mail = ResetPassMailer.reset_pass_confirmation
    assert_equal "Reset pass confirmation", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
