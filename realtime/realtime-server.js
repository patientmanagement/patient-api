var fs = require( 'fs' );
var https = require('https');
var app = require('express')();

var server = https.createServer({
		    key: fs.readFileSync('/Applications/XAMPP/xamppfiles/etc/ssl.key/server.key'),
		    cert: fs.readFileSync('/Applications/XAMPP/xamppfiles/etc/ssl.crt/server.crt')
		},app).listen(5001);
var io = require('socket.io').listen(server,{ log: false });
var redis = require('redis').createClient();

app.get('/', function(req, res) {
  res.send('hello');
});

redis.subscribe('whiteboard-session-created');
redis.subscribe('whiteboard-closed');
redis.subscribe('whiteboard-opened');

var clients = {},
    channels = {},
    rooms = {};

io.sockets.on('connection', function(socket) {
    var initiatorChannel = '';
        clients[socket.id] = socket;
    // once a client has connected, we expect to get a ping from them saying what room they want to join
    socket.on('room', function(room) {
        socket.join(room);
    });

    socket.on('message', function (data) {
        io.sockets.in(data.roomId).emit('message', data);
    });

    socket.on('presence', function (channel) {
        var isChannelPresent = !! channels[channel];
        socket.emit('presence', isChannelPresent);
    });

    socket.on('howdy', function (message) {
        console.log(message);
        socket.emit('howdy', message);
    })

    socket.on("disconnect",function(data){
	    delete clients[socket.id];
      if (initiatorChannel) {
          delete channels[initiatorChannel];
      }
	    // socket.disconnect();
	  });
});

redis.on('message', function(channel, message) {
	console.log(channel, message);
  io.sockets.in(message.room).emit(channel, JSON.parse(message));
});