class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.references :user, index: true, foreign_key: true
      t.string :name
      t.string :email
      t.string :address
      t.string :subdomain
      t.string :logo
      t.string :color
      t.string :status
      t.string :approve_key
      

      t.timestamps null: false
    end

    add_index :companies, :subdomain, unique: true
  end
end
