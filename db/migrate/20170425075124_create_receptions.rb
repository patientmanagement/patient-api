class CreateReceptions < ActiveRecord::Migration
  def change
    create_table :receptions do |t|
      ## Users
      t.integer  :staff_id
      t.integer  :user_id
      ## Receptions date
      t.datetime :start_date
      t.datetime :end_date
      ##Status
      t.integer   :status

      t.timestamps null: false
    end
  end
end
