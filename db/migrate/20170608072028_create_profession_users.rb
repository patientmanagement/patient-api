class CreateProfessionUsers < ActiveRecord::Migration
  def change
    create_table :profession_users do |t|
      t.references :profession, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
