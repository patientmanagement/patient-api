class CreateUserWorkingHours < ActiveRecord::Migration
  def change
    create_table :user_working_hours do |t|
      
      t.integer  :user_id, default: 0, null: false
      t.integer  :working_hours_id, default: 0, null: false
      t.timestamps null: false
    end
  end
end
