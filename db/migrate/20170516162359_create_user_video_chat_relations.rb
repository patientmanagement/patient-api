class CreateUserVideoChatRelations < ActiveRecord::Migration
  def change
    create_table :user_video_chat_relations do |t|
      t.integer  :user_id
      t.integer  :video_chat_id
      t.timestamps null: false
    end
  end
end
