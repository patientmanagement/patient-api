class CreatePaymentHistories < ActiveRecord::Migration
  def change
    create_table :payment_histories do |t|
      t.integer  :seller_id
      t.integer  :reception_id
      t.string  :status
      t.integer  :customer_id
      t.float :amount
      t.timestamps null: false
    end
  end
end
