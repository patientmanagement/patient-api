class CreateVideoChats < ActiveRecord::Migration
  def change
    create_table :video_chats do |t|
      t.string   :video_chat_id
      t.string   :video_chat_name
      t.integer  :is_active
      t.datetime :start_video_chat
      t.datetime :end_video_chat
      t.timestamps null: false
    end
  end
end
