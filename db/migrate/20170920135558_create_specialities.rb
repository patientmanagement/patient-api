class CreateSpecialities < ActiveRecord::Migration
  def change
    create_table :specialities do |t|
      t.references :user, index: true, foreign_key: true
      t.text :speciality

      t.timestamps null: false
    end
  end
end
