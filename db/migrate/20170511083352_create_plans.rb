class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.string :title
      t.float :price
      t.string :description
      t.string :plan_type
      t.string :color
      t.string :text_color
      t.boolean :is_logo_allowed, null: false, default: false
      t.boolean :is_color_allowed, null: false, default: false
      t.integer :staff_members, null: false, default: 0
      t.float :memory, null: false, default: 0
      
      t.timestamps null: false
    end
  end
end
