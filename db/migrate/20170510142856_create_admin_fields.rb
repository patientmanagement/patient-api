class CreateAdminFields < ActiveRecord::Migration
  def change
    create_table :admin_fields do |t|
      t.string :title

      t.timestamps null: false
    end
  end
end
