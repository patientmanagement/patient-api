class CreateRegisterFields < ActiveRecord::Migration
  def change
    create_table :register_fields do |t|
      t.integer :company_id
      t.string :title

      t.timestamps null: false
    end
  end
end
