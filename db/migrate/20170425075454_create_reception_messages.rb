class CreateReceptionMessages < ActiveRecord::Migration
  def change
    create_table :reception_messages do |t|
      t.integer  :staff_id
      t.integer  :user_id
      t.integer :sender_id
      t.integer :reception_id
      t.string :body
      t.timestamps null: false
    end
  end
end
