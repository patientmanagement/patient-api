class CreateVideoChatMessages < ActiveRecord::Migration
  def change
    create_table :video_chat_messages do |t|
    	t.references :user, index: true, foreign_key: true
    	t.references :video_chat, index: true, foreign_key: true
    	t.text :message
    	
      t.timestamps null: false
    end
  end
end
