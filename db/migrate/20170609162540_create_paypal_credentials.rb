class CreatePaypalCredentials < ActiveRecord::Migration
  def change
    create_table :paypal_credentials do |t|
      t.integer :user_id
      t.string :client_id
      t.string :client_secret

      t.timestamps null: false
    end
  end
end
