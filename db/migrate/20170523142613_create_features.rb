class CreateFeatures < ActiveRecord::Migration
  def change
    create_table :features do |t|
      t.references :plan, index: true, foreign_key: true
      t.string :title

      t.timestamps null: false
    end
  end
end
