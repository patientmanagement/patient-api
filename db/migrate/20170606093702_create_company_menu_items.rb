class CreateCompanyMenuItems < ActiveRecord::Migration
  def change
    create_table :company_menu_items do |t|
      t.references :company, index: true, foreign_key: true
      t.string :menu_item_title
      t.string :title

      t.timestamps null: false
    end
  end
end
