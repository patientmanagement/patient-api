class AddIsLoginToUsers < ActiveRecord::Migration
  def change
  	change_table :users do |t|
		  t.boolean :is_login, default: false
		end
  end
end
