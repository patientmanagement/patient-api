class CreateUserFiles < ActiveRecord::Migration
  def change
    create_table :user_files do |t|
      
      t.integer  :user_id
      t.integer  :reception_id
      t.string  :url 
      t.timestamps null: false
    end
  end
end
