class AddTokenToUsers < ActiveRecord::Migration
  def change
  	 change_table :users do |t|
		  t.text :token
		end
  end
end
