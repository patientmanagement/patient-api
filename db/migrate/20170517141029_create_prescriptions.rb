class CreatePrescriptions < ActiveRecord::Migration
  def change
    create_table :prescriptions do |t|
      t.integer  :user_id
      t.integer  :reception_id
      t.string  :message_body
      t.string  :file_url
      t.string  :pharmacy_email
      t.string :is_approved,  default: "0"
      t.timestamps null: false
    end
  end
end
