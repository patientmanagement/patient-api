class CreateWorkingHours < ActiveRecord::Migration
  def change
    create_table :working_hours do |t|
      
      t.string   :day
      t.integer   :start_time
      t.integer   :end_time
      t.timestamps null: false
    end
  end
end
