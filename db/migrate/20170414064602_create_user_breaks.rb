class CreateUserBreaks < ActiveRecord::Migration
  def change
    create_table :user_breaks do |t|
      
      t.integer  :user_id, default: 0, null: false
      t.integer  :break_id, default: 0, null: false
      t.timestamps null: false
    end
  end
end
