class CreateRegisterFieldValues < ActiveRecord::Migration
  def change
    create_table :register_field_values do |t|
      t.references :register_field, index: true, foreign_key: true
      t.integer :user_id
      t.string :value

      t.timestamps null: false
    end
  end
end
