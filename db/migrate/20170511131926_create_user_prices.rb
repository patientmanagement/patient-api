class CreateUserPrices < ActiveRecord::Migration
  def change
    create_table :user_prices do |t|
      t.integer  :user_id
      t.float :amount
      t.timestamps null: false
    end
  end
end
