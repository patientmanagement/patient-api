class CreateTwocheckoutCredentials < ActiveRecord::Migration
  def change
    create_table :twocheckout_credentials do |t|
      t.integer :user_id
      t.string :seller_id
      t.string :private_key
      t.string :publishable_key
      t.boolean :is_active, default: false

      t.timestamps null: false
    end
  end
end
