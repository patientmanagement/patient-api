class CreateUsersStaffRelations < ActiveRecord::Migration
  def change
    create_table :users_staff_relations do |t|

      t.integer  :user_id, default: 0, null: false
      t.integer  :staff_id, default: 0, null: false

      t.timestamps null: false
    end
  end
end
