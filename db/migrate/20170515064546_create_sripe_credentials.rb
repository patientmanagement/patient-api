class CreateSripeCredentials < ActiveRecord::Migration
  def change
    create_table :sripe_credentials do |t|
      t.integer  :user_id
      t.string  :key
      t.boolean :is_active, default: false
      t.timestamps null: false
    end
  end
end
