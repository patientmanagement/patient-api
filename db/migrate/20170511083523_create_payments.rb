class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :user, index: true, foreign_key: true
      t.references :credit_card, index: true, foreign_key: true
      t.references :plan, index: true, foreign_key: true
      t.float :amount
      t.string :description

      t.timestamps null: false
    end
  end
end
