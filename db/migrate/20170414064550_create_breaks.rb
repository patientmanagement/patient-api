class CreateBreaks < ActiveRecord::Migration
  def change
    create_table :breaks do |t|

      t.datetime   :start_day
      t.datetime   :end_day
      t.datetime   :end_repeat
      t.string   :repeat_type,     null: false, default: ""
      t.timestamps null: false
    end
  end
end
