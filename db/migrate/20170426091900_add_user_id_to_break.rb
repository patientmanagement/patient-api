class AddUserIdToBreak < ActiveRecord::Migration
  def change
    add_column :breaks, :user_id, :integer
  end
end
