class CreateFieldDocuments < ActiveRecord::Migration
  def change
    create_table :field_documents do |t|
      t.references :field, index: true, foreign_key: true
      t.references :document, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
