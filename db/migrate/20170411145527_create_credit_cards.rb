class CreateCreditCards < ActiveRecord::Migration
  def change
    create_table :credit_cards do |t|
      t.string   :month,     null: false, default: ""
      t.string   :year,     null: false, default: ""	
      t.string   :card_type,     null: false, default: ""	
      t.string  :number, null: false, default: "" 
      t.string  :cvc, null: false, default: ""
      t.timestamps null: false
    end
  end
end
