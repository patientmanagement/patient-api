class CreateUserFields < ActiveRecord::Migration
  def change
    create_table :user_fields do |t|
      t.references :field, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
