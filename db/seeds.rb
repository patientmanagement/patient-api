# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create({ first_name: 'Tania', last_name: 'Stoyko', email: 'tani4ka26+admin@gmail.com', user_type: 'superadmin', password: 'Aa123456', is_active: '1', is_verified: '1'})
# User.create({ first_name: 'Mike', last_name: 'Horoshko', email: 'horoshko.m+admin@gmail.com', user_type: 'superadmin', password: 'Ss123456'})
