# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171120154728) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_fields", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "breaks", force: :cascade do |t|
    t.datetime "start_day"
    t.datetime "end_day"
    t.datetime "end_repeat"
    t.string   "repeat_type", default: "", null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "user_id"
  end

  create_table "companies", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "email"
    t.string   "address"
    t.string   "subdomain"
    t.string   "logo"
    t.string   "color"
    t.string   "status"
    t.string   "approve_key"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "companies", ["subdomain"], name: "index_companies_on_subdomain", unique: true, using: :btree
  add_index "companies", ["user_id"], name: "index_companies_on_user_id", using: :btree

  create_table "company_menu_items", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "menu_item_title"
    t.string   "title"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "company_menu_items", ["company_id"], name: "index_company_menu_items_on_company_id", using: :btree

  create_table "company_users", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "company_users", ["company_id"], name: "index_company_users_on_company_id", using: :btree
  add_index "company_users", ["user_id"], name: "index_company_users_on_user_id", using: :btree

  create_table "credit_cards", force: :cascade do |t|
    t.string   "month",      default: "", null: false
    t.string   "year",       default: "", null: false
    t.string   "card_type",  default: "", null: false
    t.string   "number",     default: "", null: false
    t.string   "cvc",        default: "", null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "documents", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "features", force: :cascade do |t|
    t.integer  "plan_id"
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "features", ["plan_id"], name: "index_features_on_plan_id", using: :btree

  create_table "field_documents", force: :cascade do |t|
    t.integer  "field_id"
    t.integer  "document_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "field_documents", ["document_id"], name: "index_field_documents_on_document_id", using: :btree
  add_index "field_documents", ["field_id"], name: "index_field_documents_on_field_id", using: :btree

  create_table "fields", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payment_histories", force: :cascade do |t|
    t.integer  "seller_id"
    t.integer  "reception_id"
    t.string   "status"
    t.integer  "customer_id"
    t.float    "amount"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "payments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "credit_card_id"
    t.integer  "plan_id"
    t.float    "amount"
    t.string   "description"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "payments", ["credit_card_id"], name: "index_payments_on_credit_card_id", using: :btree
  add_index "payments", ["plan_id"], name: "index_payments_on_plan_id", using: :btree
  add_index "payments", ["user_id"], name: "index_payments_on_user_id", using: :btree

  create_table "paypal_credentials", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "client_id"
    t.string   "client_secret"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "plans", force: :cascade do |t|
    t.string   "title"
    t.float    "price"
    t.string   "description"
    t.string   "plan_type"
    t.string   "color"
    t.string   "text_color"
    t.boolean  "is_logo_allowed",  default: false, null: false
    t.boolean  "is_color_allowed", default: false, null: false
    t.integer  "staff_members",    default: 0,     null: false
    t.float    "memory",           default: 0.0,   null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "prescriptions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "reception_id"
    t.string   "message_body"
    t.string   "file_url"
    t.string   "pharmacy_email"
    t.string   "is_approved",    default: "0"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "profession_users", force: :cascade do |t|
    t.integer  "profession_id"
    t.integer  "user_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "profession_users", ["profession_id"], name: "index_profession_users_on_profession_id", using: :btree
  add_index "profession_users", ["user_id"], name: "index_profession_users_on_user_id", using: :btree

  create_table "professions", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "professions", ["company_id"], name: "index_professions_on_company_id", using: :btree

  create_table "reception_messages", force: :cascade do |t|
    t.integer  "staff_id"
    t.integer  "user_id"
    t.integer  "sender_id"
    t.integer  "reception_id"
    t.string   "body"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "receptions", force: :cascade do |t|
    t.integer  "staff_id"
    t.integer  "user_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "register_field_values", force: :cascade do |t|
    t.integer  "register_field_id"
    t.integer  "user_id"
    t.string   "value"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "register_field_values", ["register_field_id"], name: "index_register_field_values_on_register_field_id", using: :btree

  create_table "register_fields", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "specialities", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "speciality"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "specialities", ["user_id"], name: "index_specialities_on_user_id", using: :btree

  create_table "sripe_credentials", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "key"
    t.boolean  "is_active",  default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "twocheckout_credentials", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "seller_id"
    t.string   "private_key"
    t.string   "publishable_key"
    t.boolean  "is_active",       default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "user_breaks", force: :cascade do |t|
    t.integer  "user_id",    default: 0, null: false
    t.integer  "break_id",   default: 0, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "user_credit_cards", force: :cascade do |t|
    t.integer  "user_id",        default: 0, null: false
    t.integer  "credit_card_id", default: 0, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "user_documents", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "document_id"
    t.string   "filename"
    t.string   "content_type"
    t.binary   "file_contents"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "user_documents", ["document_id"], name: "index_user_documents_on_document_id", using: :btree
  add_index "user_documents", ["user_id"], name: "index_user_documents_on_user_id", using: :btree

  create_table "user_fields", force: :cascade do |t|
    t.integer  "field_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_fields", ["field_id"], name: "index_user_fields_on_field_id", using: :btree
  add_index "user_fields", ["user_id"], name: "index_user_fields_on_user_id", using: :btree

  create_table "user_files", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "reception_id"
    t.string   "url"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "user_plans", force: :cascade do |t|
    t.integer  "plan_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_plans", ["plan_id"], name: "index_user_plans_on_plan_id", using: :btree
  add_index "user_plans", ["user_id"], name: "index_user_plans_on_user_id", using: :btree

  create_table "user_prices", force: :cascade do |t|
    t.integer  "user_id"
    t.float    "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_video_chat_relations", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "video_chat_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "user_working_hours", force: :cascade do |t|
    t.integer  "user_id",          default: 0, null: false
    t.integer  "working_hours_id", default: 0, null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "user_type",              default: "",    null: false
    t.string   "first_name",             default: "",    null: false
    t.string   "logo",                   default: "",    null: false
    t.string   "last_name",              default: "",    null: false
    t.string   "is_verified",            default: "0",   null: false
    t.string   "is_active",              default: "0",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "is_login",               default: false
    t.text     "token"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_staff_relations", force: :cascade do |t|
    t.integer  "user_id",    default: 0, null: false
    t.integer  "staff_id",   default: 0, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "video_chat_messages", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "video_chat_id"
    t.text     "message"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "video_chat_messages", ["user_id"], name: "index_video_chat_messages_on_user_id", using: :btree
  add_index "video_chat_messages", ["video_chat_id"], name: "index_video_chat_messages_on_video_chat_id", using: :btree

  create_table "video_chats", force: :cascade do |t|
    t.string   "video_chat_id"
    t.string   "video_chat_name"
    t.integer  "is_active"
    t.datetime "start_video_chat"
    t.datetime "end_video_chat"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "working_hours", force: :cascade do |t|
    t.string   "day"
    t.integer  "start_time"
    t.integer  "end_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "companies", "users"
  add_foreign_key "company_menu_items", "companies"
  add_foreign_key "company_users", "companies"
  add_foreign_key "company_users", "users"
  add_foreign_key "features", "plans"
  add_foreign_key "field_documents", "documents"
  add_foreign_key "field_documents", "fields"
  add_foreign_key "payments", "credit_cards"
  add_foreign_key "payments", "plans"
  add_foreign_key "payments", "users"
  add_foreign_key "profession_users", "professions"
  add_foreign_key "profession_users", "users"
  add_foreign_key "professions", "companies"
  add_foreign_key "register_field_values", "register_fields"
  add_foreign_key "specialities", "users"
  add_foreign_key "user_documents", "documents"
  add_foreign_key "user_documents", "users"
  add_foreign_key "user_fields", "fields"
  add_foreign_key "user_fields", "users"
  add_foreign_key "user_plans", "plans"
  add_foreign_key "user_plans", "users"
  add_foreign_key "video_chat_messages", "users"
  add_foreign_key "video_chat_messages", "video_chats"
end
