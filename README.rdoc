== README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
ruby 2.4.0

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

rails s
redis-server
node realtime/realtime-server.js
bundle exec rake resque:work QUEUE=*
bundle exec rake resque:scheduler QUEUE=*


Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.
